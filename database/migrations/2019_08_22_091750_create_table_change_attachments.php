<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChangeAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_change_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('request_change_id');
            $table->string('attachments',255);
            $table->string('name',255);
            $table->string('alias',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('request_change_attachments');
    }
}
