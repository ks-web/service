<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBpsActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_change_bps_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('request_change_bps_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('action_type');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('stage_id');
            $table->timestamps();
        });
        
        Schema::create('request_change_bps_action_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('request_change_bps_action_id');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
