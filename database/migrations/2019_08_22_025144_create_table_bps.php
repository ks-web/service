<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('difficulties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('requirments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
        Schema::create('request_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('business_need');
            $table->string('business_benefit');
            $table->string('ticket', 10);
            $table->string('telp', 14);
            $table->string('location', 100);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('stage_id');
            $table->timestamps();
        });

        Schema::create('request_change_bps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('request_change_id');
            $table->unsignedInteger('priority_id');
            $table->unsignedInteger('difficult_id');
            $table->unsignedInteger('service_id');
            $table->unsignedInteger('stage_id');
            $table->unsignedInteger('status_id');
            $table->string('preparedby');
            $table->string('module');
            $table->string('email');
            $table->string('phone');
            $table->string('ticket_no', 12);
            $table->longText('bpj');
            $table->longText('global_design');
            $table->string('detail_specification');
            $table->string('effort');
            $table->date('start_plan_date');
            $table->date('finish_plan_date');
            $table->date('finish_date');
            $table->string('so_approve_name',10);
            $table->string('so_approve_status',1);
            $table->date('so_approve_date');
            $table->string('co_approve_name',10);
            $table->string('co_approve_status',1);
            $table->date('co_approve_date');
            $table->timestamps();
        });

        Schema::create('levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });

        Schema::create('request_change_bps_durations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('request_bps_id');
            $table->string('activity');
            $table->double('mandays');
            $table->string('pic');
            $table->string('level_id');
            $table->date('start_date');
            $table->date('finish_date');
            $table->timestamps();
        });

        Schema::create('request_change_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('request_change_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('action_type');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('stage_id');
            $table->timestamps();
        });
        
        Schema::create('request_change_action_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('request_change_action_id');
            $table->string('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('difficulties');
        Schema::dropIfExists('services');
    }
}
