<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Severity extends Model
{
    protected $fillable = ['name'];

    public function requests()
    {
        return $this->hasMany('App\ITRequest');
    }
}
