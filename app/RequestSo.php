<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RequestSo extends Pivot
{
    protected $table = 'request_sos';

    protected $fillable = ['request_id','service_id','status'];

    public function request()
    {
        return $this->belongsTo('App\ITRequest');
    }

    public function requestSoBps()
    {
        return $this->hasMany('App\RequestSoBps','request_sos_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }
    
}
