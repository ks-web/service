<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestSoBps extends Model
{
    protected $fillable = ['attachment','status'];

    public function requestSo()
    {
        return $this->belongsTo('App\RequestSo');
    }
}
