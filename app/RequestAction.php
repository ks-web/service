<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestAction extends Model
{
    protected $fillable = ['user','action_type'];

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_type');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function request()
    {
        return $this->belongsTo('App\ITRequest');
    }

    public function requestActionNotes()
    {
        return $this->hasOne('App\RequestActionNote');
    }
}
