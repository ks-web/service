<?php

namespace App\Exports;

use App\Models\ExportCrfModel;
use App\Models\ExportSrfModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class SrfExport implements FromView
{
    private $mulai;
    private $sampai;
    public function __construct($mulai,  $sampai)
    {
        $this->mulai = $mulai;
        $this->sampai = $sampai;
    }
    public function view(): View
    {
       if( $this->mulai==null ||  $this->sampai==null){
        return view('export_srf.excel', [
            'data' => ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->where('requests.created_at', '>', Carbon::now()->subMonths(6))->get()
	   ]);
	   }else{
		   return view('export_srf.excel', [
            'data' => ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->whereBetween('requests.created_at', [$this->mulai,$this->sampai])->get()
	   ]);
	   }
    }
}
