<?php

namespace App\Exports;

use App\Models\ExportIncidentsModel;
use App\Models\HistoryPengajuan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class IncidentsExport implements FromView
{
    private $mulai;
    private $sampai;
    public function __construct($mulai,  $sampai)
    {
        $this->mulai = $mulai;
        $this->sampai = $sampai;
    }
    public function view(): View
    {
        if( $this->mulai==null ||  $this->sampai==null){
			return view('export_incidents.excel', [
            'data' => ExportIncidentsModel::where('created_at','>', Carbon::now()->subMonths(6))->orderBy('id', 'Asc')->get()
			]);
		}else{
			return view('export_incidents.excel', [
				'data' => ExportIncidentsModel::whereBetween('created_at', [$this->mulai, $this->sampai])->orderBy('id', 'Asc')->get()
			]);
			}
    }
}
