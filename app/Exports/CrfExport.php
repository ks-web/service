<?php

namespace App\Exports;

use App\Models\ExportCrfModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CrfExport implements FromView
{
    private $mulai;
    private $sampai;
    public function __construct($mulai,  $sampai)
    {
        $this->mulai = $mulai;
        $this->sampai = $sampai;
    }
    public function view(): View
    {
        if( $this->mulai==null ||  $this->sampai==null){
			
        return view('export_crf.excel', [
            'data' => ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->where('request_changes.created_at', '>', Carbon::now()->subMonths(6))->get()
        ]);
		
		}else{
		
		return view('export_crf.excel', [
            'data' => ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->whereBetween('request_changes.created_at', [ $this->mulai,  $this->sampai])->get()
        ]);
		}
    }
}
