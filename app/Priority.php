<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $fillable = ['name'];

    public function requests()
    {
        return $this->hasMany('App\ITRequest');
    }

    public function requestsChangeBps()
    {
        return $this->hasMany('App\RequestChangeBps');
    }
}
