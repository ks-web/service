<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\KeseyaApi;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            $var = new KeseyaApi();
            $var->incident();
        })->everyMinute();

        $schedule->call(function () {
            $var = new KeseyaApi();
            $var->closeticketincident();
        })->everyMinute();

        $schedule->call(function () {
            $var = new KeseyaApi();
            $var->srf();
        })->everyMinute();

        $schedule->call(function () {
            $var = new KeseyaApi();
            $var->closeticketsrf();
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
