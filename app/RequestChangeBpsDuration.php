<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeBpsDuration extends Model
{
    protected $fillable = ['request_change_bps_id','activity','mandays','pic','level_id','start_date','finish_date'];

    public function  requestChangeBps()
    {
        return $this->belongsTo('App\RequestChangeBps');
    }
}
