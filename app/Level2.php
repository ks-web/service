<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level2 extends Model
{
     protected $table = 'level_2';
     public $timestamps = false;
     protected $primaryKey ='nik';
     protected $keyType = 'string';
}
