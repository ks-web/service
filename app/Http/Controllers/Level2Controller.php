<?php

namespace App\Http\Controllers;

use App\Level2;
use Illuminate\Http\Request;

class Level2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $level2 = Level2::latest()->paginate(5);
    
        return view('level2.index',compact('level2'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('level2.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'nik' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'telp' => 'required'
        ]);
    
        $Level2 = new Level2();
        $Level2->nama = $request->input('nama');
        $Level2->nik = $request->input('nik');
        $Level2->email = $request->input('email');
        $Level2->telp = $request->input('telp');
        $Level2->save();         
        return redirect()->route('level2.index')
                        ->with('success','User Level 2 created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Level2  $level2
     * @return \Illuminate\Http\Response
     */
    public function show(Level2 $level2)
    {
        return view('level2.show',compact('level2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Level2  $level2
     * @return \Illuminate\Http\Response
     */
    public function edit(Level2 $level2)
    {
        return view('level2.edit',compact('level2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Level2  $level2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Level2 $level2)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
        ]);
    
        $level2->update($request->all());
    
        return redirect()->route('level2.index')
                        ->with('success','User Level 2 updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Level2  $level2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Level2 $level2)
    {
        $level2->delete();
    
        return redirect()->route('level2.index')
                        ->with('success','User Level 2 deleted successfully');
    }
}
