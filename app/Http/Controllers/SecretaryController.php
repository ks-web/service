<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use UrlSigner;
use App\ITRequest;
use App\Service;
use App\Stage;
use App\Status;
use App\Category;
use App\RequestApproval;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreITRequestRequest;
use App\Http\Requests\SpictActionRequest;
use App\Http\Requests\ApprovrsaveITRequestRequest;
use App\Http\Requests\SoActionRequest;
use Illuminate\Support\Facades\Storage;
use App\Notifications\RequestCreated;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\RequestReasonfile;
use App\RequestAttachment;
use App\RequestSo;
use App\RequestSoBps;
use App\ServiceCoordinator;
use App\User;
use App\Action;
use App\RequestAction;
use App\RequestActionNote;
use Carbon\Carbon;
use App\RequestRejectattachment;
use App\Secretary;

class SecretaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countInprogressEmployee    = ITRequest::secrOfBossSubordinates()->whereNotIn('stage_id',['9','31'])->get()->count();
        $countCloseEmployee         = ITRequest::secrOfBossSubordinates()->where('stage_id','9')->get()->count();
        $countRejectedEmployee      = ITRequest::secrOfBossSubordinates()->where('stage_id','11')->get()->count();
        $paginated                  = ITRequest::secrOfBossSubordinates()->whereNotIn('stage_id',['9','31'])->get();
        $file                       = 'secretaries.index';
        $urlpath                    = 'secretary';

        return view($file,compact('paginated','userId','stage','urlpath','countInprogressEmployee','countCloseEmployee','countRejectedEmployee'));
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $secretary = Secretary::find(auth::user()->id);
        $checkBoss = User::find($secretary->boss);
        if(is_null($checkBoss))
        {
            $saveboss = new user();
            $saveboss->saveUserSecr($secretary->boss);
            $userrole = User::find($secretary->boss);
            $userrole->assignRole(['employee','boss']);

            $urlpath        = "secretary";
            $services       = Service::all();
            $secr           = Secretary::find(auth::user()->id);
            $subordinates   = $secr->getSecrPersonnelNoSubordinates($secr->boss);
            $userBoss       = User::find($secr->boss);
        }
        else
        {
            $urlpath        = "secretary";
            $services       = Service::all();
            $secr           = Secretary::find(auth::user()->id);
            $subordinates   = $secr->getSecrPersonnelNoSubordinates($secr->boss);
            $userBoss       = User::find($secr->boss);
        }
        
        if($secr->email == "divisi.beict@krakatausteel.com")
        {
            return view('secretaries.createbeict', compact('services','urlpath','subordinates','userBoss'));
        }
        else 
        {
            return view('secretaries.create', compact('services','urlpath','subordinates','userBoss'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // input data ke table request
        // $r->attachment          = $request->file('attachment')->store('attachments');
        // check employe in database
        $user = User::find($request->input('idpegawai'));
        $saveuser = new User();
        if(empty($user)) 
        {
            // check employe di api data. ada atau tidak ada
            if(!empty($saveuser->employeeCheck($request->input('idpegawai'))))
            {
                
                $secr   = new Secretary();
                $getBoss= $secr->setNikBoss($request->input('idpegawai'));
                $checkBoss = User::find($getBoss['personnel_no']);
                if(empty($checkBoss))
                {
                    // check atasan
                    if(!empty($saveuser->minManangerCheck($request->input('idpegawai'))))
                    {
                        // if tidak ada  = di input
                        $saveboss = new user();
                        $saveboss->saveMinManager($request->input('idpegawai'));
                        $bossrole = User::find($request->input('idpegawai'));
                        $bossrole->assignRole(['employee','boss']);
                    }
                    else
                    {
                        return redirect()
                            ->route('secretary.create')
                            ->with('success','Atasan tidak ditemukan di structur display.');
                    }
                }

                // input employe to database
                $saveuser = new User();
                $saveuser->saveUserSecr($request->input('idpegawai'));
                $userrole = User::find($request->input('idpegawai'));
                $userrole->assignRole(['employee']);

                
            }
            else
            {
                // jika data tidak ditemukan di structur display
                // direct ke 
                return redirect()
                    ->route('secretary.create')
                    ->with('success','Employe tidak ditemukan di struct disp.');
            }
        }
        else
        {
            // jika tidak ada di database
            if(empty($saveuser->minManangerCheck($request->input('idpegawai'))))
            {
                $saveboss = new user();
                $saveboss->saveMinManager($request->input('idpegawai'));
                $bossrole = User::find($request->input('idpegawai'));
                $bossrole->assignRole(['employee','boss']);
            }
        }
    
        $userid = User::find($request->input('idpegawai'));
        if(!empty($userid))
        {
            if(!empty($request->input('nda')))
            {
                $nda = $request->input('nda');
            }
            else
            {
                $nda = '0';
            }
            $roleso = Service::find($request->service_id)->role_id;
            $r = new ITRequest();
            $r->service_id          = $request->input('service_id');
            $r->title               = $request->input('title');
            $r->business_need       = $request->input('business_need');
            $r->business_benefit    = $request->input('business_benefit');
            $r->stage_id            = Stage::waitingBossApproval()->first()->id;
            $r->category_id         = $request->input('category');
            $r->keterangan          = $request->input('keterangan');
            $r->telp                = $request->input('telp');
            $r->location            = $request->input('location');
            $r->nda                 = $request->input('nda');
            $r->user_id             = $request->input('idpegawai');
            $r->nda_date            = Carbon::now();
            $r->so                  = $roleso;
            $r->save();
            // insert data attachment 
            if(!empty($request->file('attachment')))
            {
                foreach($request->file('attachment') as $files)
                {
                    // Carbon::parse(date_format($item['created_at'],'d/m/Y H:i:s');
                    $dateNow    = Carbon::now()->toDateTimeString();
                    $date       = Carbon::parse($dateNow)->format('dmYHis');
                    $name       = $files->getClientOriginalName();
                    $username   = $request->input('idpegawai');
                    $filename   = $date.$username.$name;
                    // upload data
                    $item = $files->storeAs('attachments', $filename);
                    // input data file
                    $r->requestAttachments()->save(new RequestAttachment(['attachment' => $item, 'name' => $filename, 'alias' => $name]));
                }
            }
            // insert data action
            $r->requestActions()->save(new RequestAction(['user' => $request->input('idpegawai'), 'action_type' => Action::Ccreate()->first()->id]));
            // insert data so
            $r->requestSos()->save(new RequestSo(['service_id' => $request->input('service_id'), 'status' => 0]));
            // return redirect
            return redirect()
                ->route('secretary.index')
                ->with('success','Permintaan layanan berhasil dibuat.');
        }
        else
        {
            return redirect()
                ->route('secretary.create')
                ->with('success','Employe tidak ditemukan didatabase.');
        }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $r, ITRequest $request, $user=NULL)
    {
        Auth::loginUsingId($user);
        $urlpath    = "request";
        $raction    = ITRequest::find($request->id)->requestActions;

        // return view('requests.show',compact('request','urlpath'));
        $requestActions = $request->requestActions->map(function($value, $item){
            return RequestAction::with(['RequestActionNotes','users','action'])
                ->find($value->id); 
        });

        $requestSos = $request->requestSos->map(function($value, $item){
            return RequestSo::with(['requestSoBps','service'])
                ->find($value->id); 
        });
        return response()->json(collect([
            'request' => $request,
                [
                    $request->service,
                    $request->category,
                    $request->stage,
                    $request->user
                ],
            'requestActions' => $requestActions,
            'requestAttachments' => $request->requestAttachments,
            'requestSos' => $requestSos,
        ]), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function subordinate()
    {
        $secr           = Secretary::find(auth::user()->id);
        $subordinates   = $secr->getSecretaryBossSubordinates();
        return response()->json($subordinates);
    }

    public function cekboss(Request $request, $id) {
        $secr   = new Secretary();
        $getBoss  = $secr->setNikBoss($id);
        return response()->json($getBoss, 200);
    }

    public function bossSubordinate(Request $request, $id)
    {
        $secr           = new Secretary();
        $subordinates   = $secr->getSecrBossSubordinates($id);
        return response()->json($subordinates);
    }
}
