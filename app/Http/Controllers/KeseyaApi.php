<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request as Reques;
use Kaseya\Client;
use GuzzleHttp\Psr7\Request as Request;
use function GuzzleHttp\json_decode;
use App\ITRequest;
use App\Stage;
use App\Incident;
use App\RequestApproval;
use App\RequestAction;
use App\IncidentApproval;
use App\IncidentAction;
use App\User;
use App\Status;
use App\Action;
use App\IncidentActionNote;
use App\requestActionNote;

class KeseyaApi extends Controller
{
    public function index()
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response   = $client->execute($request);
        echo $response->getBody();
    }

    public function notes()
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesktickets/11264282780020642768381293/notes';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true); 
        return $responsedata["Result"];

        // $response   = $client->execute($request);
        // echo $response->getBody();
    }

    public function incident()
    {
        // object incident
        $itincident         = new Incident();
        $tiketApprovedStage = array_merge(
                                    $itincident->incidentData('Level2'),
                                    $itincident->incidentData('WaitingForSpareparts'),
                                    $itincident->incidentData('Identified'),
                                    $itincident->incidentData('Level2:DD'),
                                    $itincident->incidentData('Level2:FDD'),
                                    $itincident->incidentData('Level2:SDD'),
                                    $itincident->incidentData('Solved')
                                );
        dd($tiketApprovedStage);
        // proses
        
        foreach($tiketApprovedStage as $data)
        {
            // get data incident
            $itincident     = Incident::where('ticket', $data['TicketRef'])->first();
            // cek incident data
            if(isset($itincident))
            {
                // cek stage kaseya and stage itos
                $ticketUpdate = Incident::where('ticket', $itincident->ticket)->first();
                $stage      = Stage::find($itincident->stage_id);
                $stageNew   = Stage::where('kaseya', 'like', $data['Stage'])->first();
                //dd($data['Stage'].$stage->kaseya);
                if($data['Stage'] != $stage->kaseya)
                {
                    if($ticketUpdate->breach_status == 1)
                    {
                        $breachsts = 1;
                    }
                    else
                    {
                        if($data['Stage'] == 'Level2:DD')
                        {
                            $breachsts = 1;
                        }
                        else
                        {
                            $breachsts = 0;
                        }
                    }

                    $ticketUpdate           = Incident::where('ticket', $itincident->ticket)->first();
                    $ticketUpdate->stage_id = $stageNew->id;
                    $ticketUpdate->duedate  = $data['Due'];
                    $ticketUpdate->detail = $data['Resolution'];
                    $ticketUpdate->breach_status = $breachsts;
                    $ticketUpdate->save();

                    $user = User::find(100);

                    $ra = new IncidentApproval();
                    $ra->incident_id = $ticketUpdate->id;
                    $ra->user_id     = $user->id;
                    $ra->status_id   = Status::approved()->first()->id;
                    $ra->save();
    
                    $ract = new IncidentAction();
                    $ract->incident_id   = $ticketUpdate->id;
                    $ract->user         = $user->id;
                    $ract->action_type  = Action::systemProcess()->first()->id;
                    $ract->save();

                    // $ract->incidentActionNotes()->save(new IncidentActionNote(['note' => $request->input('detail')]));
                }
            }
        }
    }

    public function srf()
    {
        // object incident
        $itincident         = new Incident();
        $tiketApprovedStage = array_merge(
                                    $itincident->incidentData('Level2'),
                                    $itincident->incidentData('WaitingForSpareparts'),
                                    $itincident->incidentData('Identified'),
                                    $itincident->incidentData('Level2:DD'),
                                    $itincident->incidentData('Level2:FDD'),
                                    $itincident->incidentData('Level2:SDD'),
                                    $itincident->incidentData('ServiceDesk'),
                                    $itincident->incidentData('Solved')
                                );
        //dd($tiketApprovedStage);
        // proses
        foreach($tiketApprovedStage as $data)
        {
            // get data incident
            $srf     = ITRequest::where('ticket', $data['TicketRef'])->first();
            // cek incident data
            if(isset($srf))
            {
                // cek stage kaseya and stage itos
                $stage      = Stage::find($srf->stage_id);
                $stageNew   = Stage::where('kaseya', 'like', $data['Stage'])->first();
                if($data['Stage'] != $stage->kaseya)
                {
                    $ticketUpdate           = ITRequest::where('ticket', $srf->ticket)->first();
                    if($ticketUpdate->breach_status == 1)
                    {
                        $breachsts = 1;
                    }
                    else
                    {
                        if($data['Stage'] == 'Level2:DD')
                        {
                            $breachsts = 1;
                        }
                        else
                        {
                            $breachsts = 0;
                        }
                    }
                   
                    $ticketUpdate->stage_id = $stageNew->id;
                    $ticketUpdate->end_date = $data['Due'];
                    $ticketUpdate->plan_date = $data['CreatedDate'];
                    $ticketUpdate->breach_status = $breachsts;
                    $ticketUpdate->save();

                    $user = User::find(100);

                    $ra = new RequestApproval();
                    $ra->request_id = $ticketUpdate->id;
                    $ra->user_id    = $user->id;
                    $ra->status_id  = Status::approved()->first()->id;
                    $ra->stage_id   = $stageNew->id;
                    $ra->save();
    
                    $ract = new RequestAction();
                    $ract->request_id   = $ticketUpdate->id;
                    $ract->user         = $user->id;
                    $ract->action_type  = Action::systemProcess()->first()->id;
                    $ract->save();
                    
                }
            }
        }
    }

    public function change()
    {
        // new object incident
        $itrequest          = new ITRequest();
        // get data tiket request change
        $tiketApprovedStage = array_merge(
                                $itrequest->requestChange('Recorded'),
                                $itrequest->requestChange('Approved'),
                                $itrequest->requestChange('Preparation'),
                                $itrequest->requestChange('Implemented'),
                                $itrequest->requestChange('Denied'),
                                $itrequest->requestChange('Change Failed'),
                                $itrequest->requestChange('Change Successful')
                            ); 
        // prosess data
        foreach($tiketApprovedStage as $data)
        {
            $itrequest  = ITRequest::where('ticket', $data['TicketRef'])->first();
            // cek data request change
            if(isset($itrequest))
            {
                // cek stage kaseya nd itos stage
                $stage      = Stage::find($itrequest->stage_id);
                $stageNew   = Stage::where('name', $data['Stage'])->first();
                if($data['Stage'] != $stage->name)
                {
                    // update stage
                    $ticketUpdate           = ITRequest::where('ticket', $itrequest->ticket)->first();
                    $ticketUpdate->stage_id = $stageNew->id;
                    $ticketUpdate->save();
                }
            }
        }
    }

    public function closeticketincident()
    {
        $newincident = new Incident();
        $incidents = Incident::whereIn('stage_id',['5','30'])->get();
        foreach($incidents as $incident)
        {
            $stage          = Stage::find($incident->stage_id);
            $kaseyaincident = $newincident->showTiket($incident->ticket);
            $stageNew       = Stage::where('kaseya', 'like', $kaseyaincident[0]['Stage'])->first();
            
            if($kaseyaincident[0]['Stage'] != $stage->kaseya)
            {
                
                $ticketUpdate           = Incident::where('ticket', $incident->ticket)->first();
                $ticketUpdate->stage_id = $stageNew->id;
                $ticketUpdate->duedate  = $kaseyaincident[0]['Due'];
                $ticketUpdate->save();

                $user = User::find(100);

                $ra = new IncidentApproval();
                $ra->incident_id = $ticketUpdate->id;
                $ra->user_id     = $user->id;
                $ra->status_id   = Status::approved()->first()->id;
                $ra->save();

                $ract = new IncidentAction();
                $ract->incident_id  = $ticketUpdate->id;
                $ract->user         = $user->id;
                $ract->action_type  = Action::systemProcess()->first()->id;
                $ract->save();
            }
        }
    }

    public function closeticketsrf()
    {
        $newsrf = new Incident();
        $srfs = ITrequest::where('stage_id','5')->where('ticket','like','%IN%')->get();
        foreach($srfs as $srf)
        {
            $stage          = Stage::find($srf->stage_id);
            $kaseyasrf      = $newsrf->showTiket($srf->ticket);
            $stageNew       = Stage::where('kaseya', 'like', $kaseyasrf[0]['Stage'])->first();
            //dd($kaseyasrf);
            // dd($stage->kaseya);
            if($kaseyasrf[0]['Stage'] != $stage->kaseya)
            {
                
                $ticketUpdate           = ITRequest::where('ticket', $srf->ticket)->first();
                $ticketUpdate->stage_id = $stageNew->id;
                $ticketUpdate->end_date = $kaseyasrf[0]['Due'];
                $ticketUpdate->plan_date = $kaseyasrf[0]['CreatedDate'];
                $ticketUpdate->save();

                $user = User::find(100);

                $ra = new RequestApproval();
                $ra->request_id  = $ticketUpdate->id;
                $ra->user_id     = $user->id;
                $ra->status_id   = Status::approved()->first()->id;
                $ra->save();

                $ract = new RequestAction();
                $ract->request_id   = $ticketUpdate->id;
                $ract->user         = $user->id;
                $ract->action_type  = Action::systemProcess()->first()->id;
                $ract->save();
            }
        }
    }
    

    public function closeticketrequest()
    {
        $newincident = new Incident();
        $incidents = Incident::where('stage_id','30')->get();
        foreach($incidents as $incident)
        {
            $stage          = Stage::find($incident->stage_id);
            $kaseyaincident = $newincident->showTiket($incident->ticket);
            $stageNew       = Stage::where('kaseya', 'like', $kaseyaincident[0]['Stage'])->first();
           
            
            if($kaseyaincident[0]['Stage'] != $stage->kaseya)
            {
                
                $ticketUpdate           = Incident::where('ticket', $incident->ticket)->first();
                $ticketUpdate->stage_id = $stageNew->id;
                $ticketUpdate->save();

                $user = User::find(100);

                $ra = new IncidentApproval();
                $ra->incident_id = $ticketUpdate->id;
                $ra->user_id     = $user->id;
                $ra->status_id   = Status::approved()->first()->id;
                $ra->save();

                $ract = new IncidentAction();
                $ract->incident_id  = $ticketUpdate->id;
                $ract->user         = $user->id;
                $ract->action_type  = Action::systemProcess()->first()->id;
                $ract->save();
            }
        }
    }

    public function procedureAgent()
    {
        // CH003325
        // IN018787
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/agentprocs';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response   = $client->execute($request);
        echo $response->getBody();
    }

    public function incidentstatus()
    {
        $masterstatus = new Incident();
        $masterstatus->incidentMastertStatus();
    }

    public function resolve()
    {
          // object incident
        $itincident         = new Incident();
        $tiketApprovedStage = array_merge(
                                    $itincident->incidentData('solved')
                                );
        //dd($tiketApprovedStage);
        // proses
        foreach($tiketApprovedStage as $data)
        {
            // get data incident
            $itincident     = Incident::where('ticket', $data['TicketRef'])->first();
            // cek incident data
            if(isset($itincident))
            {
                // cek stage kaseya and stage itos
                $stage      = Stage::find($itincident->stage_id);
                $stageNew   = Stage::where('name', $data['Stage'])->first();
                $resolution = $data['Resolution'];
                
                $ticketUpdate           = Incident::where('ticket', $itincident->ticket)->first();
                $ticketUpdate->stage_id = $stageNew->id;
                $ticketUpdate->detail   = $resolution;
                $ticketUpdate->duedate  = $data['Due'];
                
                
                $ticketUpdate->save();

                $user = User::find(100);

                $ra = new IncidentApproval();
                $ra->incident_id = $ticketUpdate->id;
                $ra->user_id    = $user->id;
                $ra->status_id  = Status::approved()->first()->id;
                $ra->save();

                $ract = new IncidentAction();
                $ract->incident_id  = $ticketUpdate->id;
                $ract->user         = $user->id;
                $ract->action_type  = Action::systemProcess()->first()->id;
                $ract->save();
                
                $ract->incidentActionNotes()->save(new IncidentActionNote(['note' => $resolution]));
            }
        }
    }

    public function resolvesrf()
    {
          // object incident
        $itincident         = new Incident();
        $tiketApprovedStage = array_merge(
                                    $itincident->incidentData('solved')
                                );
        //dd($tiketApprovedStage);
        // proses
        foreach($tiketApprovedStage as $data)
        {
            // get data incident
            $ITRequest     = ITRequest::where('ticket', $data['TicketRef'])->first();
            // cek incident data
            if(isset($ITRequest))
            {
                // cek stage kaseya and stage itos
                $stage      = Stage::find($ITRequest->stage_id);
                $stageNew   = Stage::where('name', $data['Stage'])->first();
                $resolution = $data['Resolution'];
                
                $ticketUpdate           = ITRequest::where('ticket', $ITRequest->ticket)->first();
                $ticketUpdate->stage_id = $stageNew->id;
                $ticketUpdate->detail   = $resolution;
                $ticketUpdate->end_date = $data['Due'];
                $ticketUpdate->plan_date = $data['CreatedDate'];
                $ticketUpdate->save();
                
                $user = User::find(100);

                $ra = new RequestApproval();
                $ra->request_id = $ticketUpdate->id;
                $ra->user_id    = $user->id;
                $ra->status_id  = Status::approved()->first()->id;
                $ra->stage_id   = $stageNew->id;
                $ra->save();

                $ract = new RequestAction();
                $ract->request_id   = $ticketUpdate->id;
                $ract->user         = $user->id;
                $ract->action_type  = Action::systemProcess()->first()->id;
                $ract->save();

                $ract->requestActionNotes()->save(new RequestActionNote(['note' => $resolution]));
            }
        }
    }

    public function showIncident()
    {
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/tickets?$filter=TicketRef eq \'IN020561\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        //$responsedata   = $response->getBody(); 
        return $responsedata["Result"];
    }

    public function showsrf()
    {
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/tickets?$top=50&$orderby=TicketRef desc';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        //$responsedata   = $response->getBody(); 
        return $responsedata["Result"];
    }
    
}

