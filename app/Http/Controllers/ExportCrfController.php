<?php

namespace App\Http\Controllers;

use App\Exports\CrfExport;
use App\Models\ExportCrfModel;
use App\RequestChangeBps;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ExportCrfController extends Controller
{
    public function index(Request $request)
    {
        $urlpath = 'exportCrf';
			 $mulai = null;
            $sampai = null;
        if ($request->mulai == null || $request->sampai == null) {
            $data = ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->where('request_changes.created_at', '>', Carbon::now()->subMonths(6))->get();
           
        } else {
            $mulai = date('Y-m-d', strtotime($request->mulai));
            $sampai = date('Y-m-d', strtotime($request->sampai));
            $data = ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->whereBetween('request_changes.created_at', [$mulai, $sampai])->get();
        }
        return view('export_crf.index', compact('data', 'urlpath', 'mulai', 'sampai'));
    }

    public function cetak_pdf(Request $request)
    {
        
        if ($request->mulai == null || $request->sampai == null) {
            $data =  ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->where('request_changes.created_at', '>', Carbon::now()->subMonths(6))->get();
            $pdf = PDF::loadview('export_crf.pdf', ['data' => $data])->setPaper('Legal', 'landscape');;
            return $pdf->stream();
        } else {
			$mulai = date('Y-m-d', strtotime($request->mulai));
        $sampai = date('Y-m-d', strtotime($request->sampai));
            $data = ExportCrfModel::join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id')->whereBetween('request_changes.created_at', [$mulai, $sampai])->get();
            $pdf = PDF::loadview('export_crf.pdf', ['data' => $data])->setPaper('Legal', 'landscape');;
            return $pdf->stream();
        }
    }

    public function export(Request $request)
    {
        if($request->mulai!=null || $request->sampai!=null){
			
			$mulai = date('Y-m-d H:i:s', strtotime($request->mulai));
			$sampai = date('Y-m-d H:i:s', strtotime($request->sampai));
			
		}else{
			$mulai = $request->mulai;
			$sampai = $request->sampai;
		}
        return Excel::download(new CrfExport($mulai, $sampai), 'crf.xlsx');
    }
}
