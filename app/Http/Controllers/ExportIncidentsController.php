<?php

namespace App\Http\Controllers;

use App\Exports\IncidentsExport;
use Illuminate\Http\Request;
use App\Models\ExportIncidentsModel;
use PDF;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ExportIncidentsController extends Controller
{
    public function index(Request $request)
    {
        $urlpath = 'exportIncidents';
		 $mulai = null;
         $sampai = null;

        if ($request->mulai == null || $request->sampai == null) {
            $data = ExportIncidentsModel::where('created_at', '>', Carbon::now()->subMonths(6))->get();
            
        } else {
            $mulai = date('Y-m-d', strtotime($request->mulai));
            $sampai = date('Y-m-d', strtotime($request->sampai));
            $data = ExportIncidentsModel::whereBetween('created_at', [$mulai, $sampai])->orderBy('id', 'Asc')->get();
        }
        return view('export_incidents.index', compact('data', 'urlpath', 'mulai', 'sampai'));
    }

    public function cetak_pdf(Request $request)
    {
        if ($request->mulai == null || $request->sampai == null) {
            $data = ExportIncidentsModel::where('created_at', '>', Carbon::now()->subMonths(6))->get();
            
        } else {
			$mulai = date('Y-m-d', strtotime($request->mulai));
			$sampai = date('Y-m-d', strtotime($request->sampai));
            $data = ExportIncidentsModel::whereBetween('created_at', [$mulai, $sampai])->get();
        }
            $pdf = PDF::loadview('export_incidents.pdf', ['data' => $data])->setPaper('A4', 'landscape');;
            return $pdf->stream();
    }

    public function export(Request $request)
    {
		//dd($request->mulai);
       if($request->mulai!=null || $request->sampai!=null){
			
			$mulai = date('Y-m-d H:i:s', strtotime($request->mulai));
			$sampai = date('Y-m-d H:i:s', strtotime($request->sampai));
			
		}else{
			$mulai = $request->mulai;
			$sampai = $request->sampai;
		}
        return Excel::download(new IncidentsExport($mulai, $sampai), 'incidents.xlsx');
    }
}
