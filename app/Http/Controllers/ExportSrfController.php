<?php

namespace App\Http\Controllers;

use App\Exports\CrfExport;
use App\Exports\SrfExport;
use App\Models\ExportSrfModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class ExportSrfController extends Controller
{
    public function index(Request $request)
    {

        $urlpath = 'exportSrf';

            $mulai = null;
            $sampai = null;
        if ($request->mulai == null || $request->sampai == null) {
            $data = ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->where('requests.created_at', '>', Carbon::now()->subMonths(6))->get();
        } else {
            $mulai = date('Y-m-d', strtotime($request->mulai));
            $sampai = date('Y-m-d', strtotime($request->sampai));
            $data = ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->whereBetween('requests.created_at', [$mulai, $sampai])->get();
        }
        return view('export_srf.index', compact('data', 'urlpath', 'mulai', 'sampai'));
    }

    public function cetak_pdf(Request $request)
    {
        if ($request->mulai == null || $request->sampai == null){
			$data = ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->where('requests.created_at','>', Carbon::now()->subMonths(6))->get();
		}else{
			
		$mulai = date('Y-m-d', strtotime($request->mulai));
        $sampai = date('Y-m-d', strtotime($request->sampai));

        $data = ExportSrfModel::join('request_attachments', 'requests.id', '=', 'request_attachments.request_id')->whereBetween('requests.created_at', [$mulai, $sampai])->get();
		}
        $pdf = PDF::loadview('export_srf.pdf', ['data' => $data])->setPaper('A4', 'landscape');;
        return $pdf->stream();
    }

    public function export(Request $request)
    {
		if($request->mulai!=null || $request->sampai!=null){
			
			$mulai = date('Y-m-d H:i:s', strtotime($request->mulai));
			$sampai = date('Y-m-d H:i:s', strtotime($request->sampai));
			
		}else{
			$mulai = $request->mulai;
			$sampai = $request->sampai;
		}
        return Excel::download(new SrfExport($mulai, $sampai), 'srf.xlsx');
    }
}
