<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestChange extends Model
{
    protected $fillable = ['finish_plan_date', 'service_id', 'title', 'business_need', 'business_benefit', 'ticket', 'telp', 'location', 'user_id', 'stage_id', 'action_id', 'boss'];
    //protected $table="v_crf";
    // relation with RequestChangeAttachments
    public function requestChangeAttachments()
    {
        return $this->hasMany('App\RequestChangeAttachment', 'request_change_id');
    }

    public function RequestChangeRejectAttachments()
    {
        return $this->hasMany('App\RequestChangeRejectAttachment', 'request_change_id');
    }

    public function requestChangeBps()
    {
        return $this->hasOne('App\RequestChangeBps', 'request_change_id');
    }

    public function requestChangeActions()
    {
        return $this->hasMany('App\RequestChangeAction');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function action()
    {
        return $this->belongsTo('App\Action');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeOfSemua($query)
    {
        $query->select('request_changes.id', 'request_changes.service_id', 'request_changes.title', 'request_changes.business_need', 'request_changes.business_benefit', 'request_changes.ticket', 'request_changes.telp', 'request_changes.location', 'request_changes.user_id', 'request_changes.stage_id', 'request_changes.action_id', 'request_changes.boss', 'request_changes.created_at', 'request_changes.updated_at', 'request_change_bps.finish_plan_date');
        $query->leftjoin('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id');
        $query->where('request_changes.service_id', '!=', '0');
    }

    public function scopeOfSo($query, $serv)
    {
        $query->select('request_changes.id', 'request_changes.service_id', 'request_changes.title', 'request_changes.business_need', 'request_changes.business_benefit', 'request_changes.ticket', 'request_changes.telp', 'request_changes.location', 'request_changes.user_id', 'request_changes.stage_id', 'request_changes.action_id', 'request_changes.boss', 'request_changes.created_at', 'request_changes.updated_at', 'request_change_bps.finish_plan_date');
        $query->join('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id');
        $query->whereIn('request_changes.service_id', $serv);
        $query->orWhere('service_id', '17');
    }
    public function scopeOfLoggedUser($query)
    {
        $query->select('request_changes.id', 'request_changes.service_id', 'request_changes.title', 'request_changes.business_need', 'request_changes.business_benefit', 'request_changes.ticket', 'request_changes.telp', 'request_changes.location', 'request_changes.user_id', 'request_changes.stage_id', 'request_changes.action_id', 'request_changes.boss', 'request_changes.created_at', 'request_changes.updated_at', 'request_change_bps.finish_plan_date');
        $query->leftjoin('request_change_bps', 'request_changes.id', '=', 'request_change_bps.request_change_id');
        $query->where('user_id', Auth::user()->id);
    }

    public function scopeOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getPersonnelNoSubordinates());
    }

    public function scopeSecrOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getBossSubordinates());
    }
}
