<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\RequestObserver;
use App\Observers\IncidentObserver;
use App\Observers\RequestApprovalObserver;
use App\Observers\IncidentApprovalObserver;
use App\Observers\RequestChangeObserver;
use App\Observers\RequestChangeActionObserver;
use App\Observers\RequestChangeBpsActionObserver;
use App\Observers\RequestChangeBpsObserver;
use Illuminate\support\Facades\Schema;
use App\ITRequest;
use App\RequestApproval;
use App\Incident;
use App\IncidentApproval;
use App\RequestChange;
use App\RequestChangeAction;
use App\RequestChangeBps;
use App\RequestChangeBpsAction;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // https
        if(config('app.env') === 'production'){
          \Illuminate\Support\Facades\URL::forceScheme('https');     
        }    
        // Schema::defaultStringLength(191);
        ITRequest::observe(RequestObserver::class);
        Incident::observe(IncidentObserver::class);
        RequestApproval::observe(RequestApprovalObserver::class);
        IncidentApproval::observe(IncidentApprovalObserver::class);
        RequestChange::observe(RequestChangeObserver::class);
        RequestChangeBpsAction::observe(RequestChangeBpsActionObserver::class);
        RequestChangeAction::observe(RequestChangeActionObserver::class);
        RequestChangeBps::observe(RequestChangeBpsObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
