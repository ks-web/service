<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Kaseya\Client;
use GuzzleHttp\Psr7\Request as Request;
use function GuzzleHttp\json_decode;

class Incident extends Model
{
    // public function __construct(){
    //     parent::__construct();
    //     $user = new User();
    //     //dd(preg_match("/^(\/)$/", 'dsds/sdsd/'));
    //     if($user->getId() !== null)
    //     {
    //         if((preg_match("/incident\/show/", request()->path()) == 0) && (preg_match("/^(\/)$/", request()->path()) == 0) && (preg_match("/srf\/resolve/", request()->path()) == 0) && (preg_match("/srf/", request()->path()) == 0) && (preg_match("/kaseya\/incident/", request()->path()) == 0) && (preg_match("/resolve/", request()->path()) == 0))
    //         {
    //             $user = User::find($user->getId());
    //             $user->last_activity = request()->path();
    //             $user->save();
    //         }
    //     }
    // }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function priority()
    {
        return $this->belongsTo('App\Priority');
    }

    public function severity()
    {
        return $this->belongsTo('App\Severity');
    }

    public function incidentApprovals()
    {
        return $this->hasMany('App\IncidentApproval');
    }

    public function incidentActions()
    {
        return $this->hasMany('App\IncidentAction')->orderBy('id','DESC');
    }

    public function incidentAttachments()
    {
        return $this->hasMany('App\IncidentAttachment', 'incident_id');
    }

    public function incidentRejectattachments()
    {
        return $this->hasMany('App\incidentRejectattachment', 'incident_id');
    }

    public function scopeOfLoggedUser($query)
    {
        $query->where('user_id', Auth::user()->id);
    }

    public function scopeOfCountUserInProgress($query)
    {
        $query->where('user_id', Auth::user()->id)->whereNotIn('stage_id',['11','9','31']);
    }
    
    public function incidentData($stage = NULL)
    {
        // 
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/tickets?$top=50&$filter=Stage eq \''.$stage.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        //$responsedata   = $response->getBody(); 
        return $responsedata["Result"];
    }

    public function incidentByStatus($status = NULL)
    {
        // 
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/tickets?$top=50&$filter=substringof(\''.$status.'\',TicketStatus)';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        //$responsedata   = $response->getBody(); 
        return $responsedata["Result"];
    }

    public function closeTicket($ticketId = NULL, $statusId = NULL)
    {
        //automation/servicedesktickets/{ticketId}/status/{statusId}
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'PUT';
        $actionUri  = '/automation/servicedesktickets/'.$ticketId.'/status/'.$statusId;
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        return "Tiket Close";
        //$responsedata   = $response->getBody(); 

        // // new object incident Example
        // $itincident = new Incident();
        // // get status
        // $showstatus = $itincident->incidentStatus('Closed');
        // $datastatus = array_column($showstatus,'StatusId');
        // // get tiket
        // $showtiket  = $itincident->showTiket('IN018787');
        // $datatiket  = array_column($showtiket, 'ServiceDeskTicketId');
        // // close tiket
        // $itincident->closeTicket($datatiket[0],$datastatus[0]);
    }

    public function showTiket($reference = NULL)
    {
        // 
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");
        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/tickets?$top=50&$filter=TicketRef eq \''.$reference.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;
        
        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true); 
        return $responsedata["Result"];
    }

    public function incidentStatus($status = NULL)
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/status?$filter=StatusName eq \''.$status.'\'';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response       = $client->execute($request);
        $responsedata   = json_decode($response->getBody(), true);
        return $responsedata["Result"];
    }

    public function incidentMastertStatus()
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/70875525840011435883993088/status';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response   = $client->execute($request);
        echo $response->getBody();
    }

    public function requestMasterStage()
    {
        // CH003300
        // automation/servicedesks/{serviceDeskId}/categories
        $protocol   = 'https://';
        $host       = 'servicedesk.krakatausteel.com';
        $basePath   = '/api/v1.0';
        $client     = new Client($host, "tim.web", "tim.web2019");

        $httpMethod = 'GET';
        $actionUri  = '/automation/servicedesks/87132147890095585806341772/stage';
        $postBody   = false;
        $url        = $protocol . $host . $basePath . $actionUri;

        $request    = new Request(
            $httpMethod,
            $url,
            ['content-type' => 'application/json'],
            $postBody ? json_encode($postBody) : ''
        );

        $response   = $client->execute($request);
        echo $response->getBody();
    }
}
