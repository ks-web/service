<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\RequestChange;
use App\RequestChangeBps;
use App\RequestChangeBpsAction;
use App\RequestChangeAction;

class RequestChangeCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $requestChange;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(RequestChangeAction $requestchangeaction)
    {
        //
        $this->requestChange = $requestchangeaction->requestChange;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Dengan Hormat')
            ->subject($this->requestChange->title)
            ->line('Nama :' . $this->requestChange->user->name . "(" . $this->requestChange->user->id . ")")
            ->line('id :' . $this->requestChange->id)
            ->line('Kebutuhan Bisnis: ' . $this->requestChange->business_need)
            ->line('Manfaat Bisnis: ' . $this->requestChange->business_benefit)
            ->line($this->requestChange->stage->name)
            // ->action('Notification Action', $urlVar)
            ->line('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
