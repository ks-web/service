<?php

namespace App\Notifications;

use App\RequestApproval;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class RequestCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $request;
    public $url;
    public $urldb;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(RequestApproval $requestApproval, $_url=NULL, $_urldb="#")
    {
        $this->request  = $requestApproval->request;
        $this->url      = $_url;
        $this->urldb    = $_urldb;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $nda;
        if ($this->request->stage->id == 6) {
            //$url = route('requests.approveshow', $this->request->id);
            $urlVar = $this->url;
            return (new MailMessage)
                    ->greeting('Dengan Hormat')
                    ->subject($this->request->title)
                    ->line('Nama :' . $this->request->user_id . "(" . $this->request->user->name . ")")
                    ->line('id :' . $this->request->id)
                    ->line('Kebutuhan Bisnis: ' . $this->request->business_need)
                    ->line('Manfaat Bisnis: ' . $this->request->business_benefit)
                    ->line($this->request->stage->name)
                    ->line('This nda nda nda nda nda')
                    // ->action('Notification Action', $urlVar)
                    ->line('Thank you');
        }
        else
        {
                
            if ($this->request->stage->id == 1) {
                $urlVar = $this->url; //route('requests.approveshow', $this->request->id);
            } elseif ($this->request->stage->id == 2) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 7) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 10) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 3) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 4) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 13) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 12) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 16) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 18) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 17) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 9) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 11) {
                $urlVar = $this->url;
            }  elseif ($this->request->stage->id == 28) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 14) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 19) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 30) {
                $urlVar = $this->url;
            } elseif ($this->request->stage->id == 5) {
                $urlVar = $this->url;
            }
             else{
                $urlVar = '';
            }

            return (new MailMessage)
                    ->greeting('Dengan Hormat')
                    ->subject($this->request->title)
                    ->line('Nama :' . $this->request->user_id . "(" . $this->request->user->name . ")")
                    ->line('id :' . $this->request->id)
                    ->line('Kebutuhan Bisnis: ' . $this->request->business_need)
                    ->line('Manfaat Bisnis: ' . $this->request->business_benefit)
                    ->line($this->request->stage->name)
                    // ->action('Notification Action', $urlVar)
                    ->line('Thank you');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if ($this->request->stage->id == 1) {
            $urlVar = $this->urldb; //route('requests.approveshow', $this->request->id);
        } elseif ($this->request->stage->id == 2) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 7) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 10) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 3) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 4) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 13) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 12) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 16) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 18) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 17) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 6) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 9) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 11) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 28) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 14) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 19) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 30) {
            $urlVar = $this->urldb;
        } elseif ($this->request->stage->id == 5) {
            $urlVar = $this->urldb;
        }else{
            $urlVar = '';
        }

        return [
            'stage_id' => $this->request->stage_id,
            'id' => $this->request->id,
            'business_benefit' => str_limit($this->request->business_need, 50),
            'url' => $urlVar,
        ];
    }
}
