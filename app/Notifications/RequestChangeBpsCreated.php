<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\RequestChangeBpsAction;
use App\RequestChangeBps;

class RequestChangeBpsCreated extends Notification implements ShouldQueue
{
    use Queueable;
    public $requestChangeBps;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(RequestChangeBpsAction $RequestChangeBpsAction)
    {
        //
        $this->requestChangeBps = $RequestChangeBpsAction->requestChangeBps;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Dengan Hormat')
            ->subject($this->requestChangeBps->requestChange->title)
            ->line('Nama :' . $this->requestChangeBps->requestChange->user->name . "(" . $this->requestChangeBps->requestChange->user->id . ")")
            ->line('id :' . $this->requestChangeBps->requestChange->id)
            ->line('Kebutuhan Bisnis: ' . $this->requestChangeBps->requestChange->business_need)
            ->line('Manfaat Bisnis: ' . $this->requestChangeBps->requestChange->business_benefit)
            ->line($this->requestChangeBps->stage->name)
            // ->action('Notification Action', $urlVar)
            ->line('Thank you');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
