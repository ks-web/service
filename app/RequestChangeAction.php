<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeAction extends Model
{
    protected $fillable = ['request_change_id','user_id','action_type','status_id','stage_id','role_id'];
    
    public function requestChangeActionNotes()
    {
        return $this->hasOne('App\RequestChangeActionNote');
    }

    public function requestChange()
    {
        return $this->belongsTo('App\RequestChange');
    }

    public function Role()
    {
        return $this->belongsTo('App\Role');
    }

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_type')->orderBy('id','DESC');;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
   
}
