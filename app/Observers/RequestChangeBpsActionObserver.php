<?php

namespace App\Observers;

use App\RequestChange;
use App\RequestChangeBps;
use App\Status;
use App\User;
use App\Stage;
use App\Service;
use App\Action;
use App\RequestChangeBpsAction;
use App\ServiceCoordinator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Notifications\RequestChangeCreated;
use App\Notifications\RequestChangeBpsCreated;

class RequestChangeBpsActionObserver
{
    /**
     * Handle the request "created" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function created(RequestChangeBpsAction $rcbpsaction)
    {
        if($rcbpsaction->requestChangeBps != null)
        {
            if($rcbpsaction->requestChangeBps->stage_id == Stage::waitingForServiceDesk()->first()->id)
            {
                // notifikasi 
                $role       = Role::findByName('service desk');
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::waitingForBps()->first()->id)
            {
                // notifikasi 
                $service_so = $rcbpsaction->requestChangeBps;
                $servid     = $service_so->service_id;
                $roleid     = Service::where('id',$servid)->first();
                $role       = Role::where('id', $roleid->role_id)->first();
                
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::updateBps()->first()->id)
            {
                // notifikasi 
                $service_so = $rcbpsaction->requestChangeBps;
                $servid     = $service_so->service_id;
                $roleid     = Service::where('id',$servid)->first();
                $role       = Role::where('id', $roleid->role_id)->first();
                
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::waitingForCoordinatorSo()->first()->id)
            {
                // notifikasi 
                $service_so = $rcbpsaction->requestChangeBps->requestChange;
                $servid     = $service_so->service_id;
                $serco      = ServiceCoordinator::where('service_id', $servid)->first();
                $role       = Role::where('id', $serco->role_id)->first();
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::waitingForManagerBeict()->first()->id)
            {
                // notifikasi 
                $role       = Role::findByName('manager beict');
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::approveKaseya()->first()->id)
            {
                // notifikasi 
                $role       = Role::findByName('operation sd');
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
            elseif($rcbpsaction->requestChangeBps->stage_id == Stage::requestRejected()->first()->id)
            {
                // notifikasi 
                $service_so = $rcbpsaction->requestChangeBps;
                $servid     = $service_so->service_id;
                $roleid     = Service::where('id',$servid)->first();
                $role       = Role::where('id', $roleid->role_id)->first();
                
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
        }
        else 
        {
            if($rcbpsaction->stage_id == Stage::waitingForCoordinatorSo()->first()->id)
            {
                // notifikasi 
                // dd($rcbpsaction->requestChangeBps);
                $service_so = $rcbpsaction->requestChangeBps->requestChange;
                $servid     = $service_so->service_id;
                $serco      = ServiceCoordinator::where('service_id', $servid)->first();
                $role       = Role::where('id', $serco->role_id)->first();
                $role       = Role::findByName($role->name);
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($rcbpsaction) {
                    $item->notify(new RequestChangeBpsCreated($rcbpsaction));
                });
            }
        }

    }

    /**
     * Handle the request "updated" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function updated(RequestChangeBpsAction $requestchange, Request $request)
    {
       
    }

    /**
     * Handle the request "deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function deleted(RequestChangeBpsAction $request)
    {
        //
    }

    /**
     * Handle the request "restored" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function restored(RequestChangeBpsAction $request)
    {
        //
    }

    /**
     * Handle the request "force deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function forceDeleted(RequestChangeBpsAction $request)
    {
        //
    }
}
