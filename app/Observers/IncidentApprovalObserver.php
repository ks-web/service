<?php

namespace App\Observers;

use App\IncidentApproval;
use App\Notifications\IncidentCreated;
use App\User;
use App\Incident;
use UrlSigner;
use Spatie\Permission\Models\Role;

class IncidentApprovalObserver
{
    /**
     * Handle the incident approval "created" event.
     *
     * @param  \App\IncidentApproval  $incidentApproval
     * @return void
     */
    public function created(IncidentApproval $incidentApproval)
    {
        if($incidentApproval->incident->stage_id == 3)
        {
            $role = Role::findByName('service desk');
            $collection = $role->users;

            $collection->each(function ($item, $key) use ($incidentApproval) {
                $urldb = route('incidents.ticketshow',$incidentApproval->incident->id);
                $url = UrlSigner::sign(route('signed.incidents.ticketshow',[$incidentApproval->incident->id, $item->id]),2);
                $item->notify(new IncidentCreated($incidentApproval, $url, $urldb));
            });
            
        }
        else if($incidentApproval->incident->stage_id == 6)
        {
            $url = UrlSigner::sign(route('signed.incidents.approveshow',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
            $urldb = route('incidents.approveshow', $incidentApproval->incident->id);
            $user = User::find($incidentApproval->incident->user_id);
            $user->notify(new IncidentCreated($incidentApproval, $url, $urldb));
        }
        else if($incidentApproval->incident->stage_id == 9)
        {
            $role = Role::findByName('service desk');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($incidentApproval) {
                $urldb = route('incidents.show',$incidentApproval->incident->id);
                $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
                $item->notify(new IncidentCreated($incidentApproval, $url, $urldb));
            });
        }
        else if($incidentApproval->incident->stage_id == 4)
        {
            $user = User::find($incidentApproval->incident->user_id);
            $urldb = route('incidents.show',$incidentApproval->incident->id);
            $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
            $user->notify(new IncidentCreated($incidentApproval, $url, $urldb));
        }
        else if($incidentApproval->incident->stage_id == 5)
        {
            $user = User::find($incidentApproval->incident->user_id);
            $urldb = route('incidents.show',$incidentApproval->incident->id);
            $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
            $user->notify(new IncidentCreated($incidentApproval, $url, $urldb));
        }
        else if($incidentApproval->incident->stage_id == 30)
        {
            $user = User::find($incidentApproval->incident->user_id);
            $urldb = route('incidents.show',$incidentApproval->incident->id);
            $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
            $user->notify(new IncidentCreated($incidentApproval, $url, $urldb));
        }
        else if($incidentApproval->incident->stage_id == 2)
        {
            $role = Role::findByName('operation sd');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($incidentApproval) {
                $urldb = route('incidents.show',$incidentApproval->incident->id);
                $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
                $item->notify(new IncidentCreated($incidentApproval, $url, $urldb));
            });
        }
        else if($incidentApproval->incident->stage_id == 38)
        {
            $user = User::find($incidentApproval->incident->user_id);
            $urldb = route('incidents.show',$incidentApproval->incident->id);
            $url = UrlSigner::sign(route('signed.incidents.show',[$incidentApproval->incident->id, $incidentApproval->incident->user_id]),2);
            $user->notify(new IncidentCreated($incidentApproval, $url, $urldb));
        }
    }

    /**
     * Handle the incident approval "updated" event.
     *
     * @param  \App\IncidentApproval  $incidentApproval
     * @return void
     */
    public function updated(IncidentApproval $incidentApproval)
    {
        //
    }

    /**
     * Handle the incident approval "deleted" event.
     *
     * @param  \App\IncidentApproval  $incidentApproval
     * @return void
     */
    public function deleted(IncidentApproval $incidentApproval)
    {
        //
    }

    /**
     * Handle the incident approval "restored" event.
     *
     * @param  \App\IncidentApproval  $incidentApproval
     * @return void
     */
    public function restored(IncidentApproval $incidentApproval)
    {
        //
    }

    /**
     * Handle the incident approval "force deleted" event.
     *
     * @param  \App\IncidentApproval  $incidentApproval
     * @return void
     */
    public function forceDeleted(IncidentApproval $incidentApproval)
    {
        //
    }
}
