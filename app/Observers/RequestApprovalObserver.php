<?php

namespace App\Observers;

use App\RequestApproval;
use App\Notifications\RequestCreated;
use App\Notifications\RequestBossApproved;
use App\User;
use App\Status;
use App\ITRequest;
use Illuminate\Support\Facades\Auth;
use App\Stage;
use Spatie\Permission\Models\Role;
use App\Service;
use App\ServiceCoordinator;
use App\RequestSo;
use UrlSigner;
use App\Secretary;

class RequestApprovalObserver
{
    /**
     * Handle the request approval "created" event.
     *
     * @param  \App\RequestApproval  $requestApproval
     * @return void
     */

    public function created(RequestApproval $requestApproval)
    {
        
        // waiting boss approve stage 1
        if($requestApproval->request->stage_id == Stage::waitingBossApproval()->first()->id)
        { 
            //UrlSigner::sign(route('signed.bossapprove', [1,4]), 30);
            if(Auth::guard('secr')->check())
            {
                $secr   = new Secretary();
                if(sizeof($userBoss->minManangerCheck($requestApproval->request->user_id)) != 0)
                {
                    $getBoss= $secr->setNikBoss($requestApproval->request->user_id);
                }
                else
                {
                    if(sizeof($userBoss->minManangerDelegationCheck($requestApproval->request->user_id)) != 0)
                    {
                        $getBoss    = $userBoss->setNikDelegationBoss($$requestApproval->request->user_id);
                    }
                    else
                    {
                        return "Atasan anda tidak terdaftar di struktur display silahkan hubungi 7 2743";
                    }
                }
                
                $boss   = User::find($getBoss['personnel_no']);

                $url    = UrlSigner::sign(route('signed.approveshow', [$requestApproval->request->id,  $boss->id]),2);
                $urldb  = route('requests.approveshow', $requestApproval->request->id);
                $boss->notify(new RequestCreated($requestApproval, $url, $urldb));
            }
            else
            {
                $boss   = Auth::user()->boss();
                $url    = UrlSigner::sign(route('signed.approveshow', [$requestApproval->request->id, Auth::user()->boss()->id]),2);
                $urldb  = route('requests.approveshow', $requestApproval->request->id);
                $boss->notify(new RequestCreated($requestApproval, $url, $urldb));
            }
        }
        // waiting operation desc stage 2
        else if($requestApproval->request->stage_id == Stage::waitingForOperationDesk()->first()->id)
        {
            // notifikasi operation sd
            $role       = Role::findByName('operation sd');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.spsdshow', [$requestApproval->request->id, $item->id]),2);
                $urldb  = route('requests.spsdshow', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
            // notifikasi operation sptict
            $role       = Role::findByName('operation ict');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2); 
                $urldb  = route('requests.show', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
            // notifikasi manager
            $role1          = Role::findByName('manager beict');
            $collection1    = $role1->users;
            $collection1->each(function ($item, $key) use ($requestApproval) {
                $url        = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2); 
                $urldb      = route('requests.show', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url,$urldb));
            });

        }
        // stage operation ict
        else if($requestApproval->request->stage_id == Stage::waitingForOperationIct()->first()->id)
        {
            $role       = Role::findByName('operation ict');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.approveshow', [$requestApproval->request->id, $item->id]),2);
                $urldb  = route('requests.approveshow', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
        }
        // chief 
        else if($requestApproval->request->stage_id == Stage::waitingForChiefApproval()->first()->id)
        {
            $role       = Role::findByName('chief');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.chiefshow', [$requestApproval->request->id, $item->id]),2);
                $urldb  = route('requests.chiefshow', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
        }
        // waiting operation desc stage 3
        else if($requestApproval->request->stage_id == Stage::waitingForServiceDesk()->first()->id)
        {
            if($requestApproval->request->category_id == 1)
            {
                $role       = Role::findByName('service desk');
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.editticket', [$requestApproval->request->id, $item->id]),2);
                    $urldb  = route('requests.editticket', $requestApproval->request->id);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
            else
            {
                // notifikasi to user request
                $user   = User::find($requestApproval->request->user_id);
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
                $user->notify(new RequestCreated($requestApproval, $url, $urldb));

                // notifikasi to service desk
                $role       = Role::findByName('service desk');
                $collection = $role->users;
                $collection->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.editticket', [$requestApproval->request->id, $item->id]),2);
                    $urldb  = route('requests.editticket', $requestApproval->request->id);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
        }
        // solved
        else if($requestApproval->request->stage_id == Stage::resolve()->first()->id)
        {
            // cek berdasarkan kategori
            if($requestApproval->request->category_id == 1)
            {
                $user   = User::find($requestApproval->request->user_id);
                $boss   = $user->boss();
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $boss->id]),2); 
                $boss->notify(new RequestCreated($requestApproval, $url, $urldb));

                $user1  = User::find($requestApproval->request->user_id);
                $urldb1 = route('requests.show', $requestApproval->request->id);
                $url1   = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2); 
                $user1->notify(new RequestCreated($requestApproval, $url1, $urldb1));
            }
            else
            {
                // notifikasi ke user and boss
                $user   = User::find($requestApproval->request->user_id);
                $boss   = $user->boss();
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $boss->id]),2); 
                $boss->notify(new RequestCreated($requestApproval,$url, $urldb));
    
                $user1  = User::find($requestApproval->request->user_id);
                $urldb1 = route('requests.show', $requestApproval->request->id);
                $url1   = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2); 
                $user1->notify(new RequestCreated($requestApproval,$url1, $urldb1));
            }
        }
        
        // stage waiting user confirmation
        else if($requestApproval->request->stage_id == Stage::waitingUserConf()->first()->id)
        {
            // cek berdasarkan kategori
            if($requestApproval->request->category_id == 1)
            {
                $user   = User::find($requestApproval->request->user_id);
                $boss   = $user->boss();
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $boss->id]),2); 
                $boss->notify(new RequestCreated($requestApproval, $url, $urldb));

                $user1  = User::find($requestApproval->request->user_id);
                $urldb1 = route('requests.show', $requestApproval->request->id);
                $url1   = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2); 
                $user1->notify(new RequestCreated($requestApproval, $url1, $urldb1));
            }
            else
            {
                // notifikasi ke user and boss
                $user   = User::find($requestApproval->request->user_id);
                $boss   = $user->boss();
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $boss->id]),2); 
                $boss->notify(new RequestCreated($requestApproval,$url, $urldb));
    
                $user1  = User::find($requestApproval->request->user_id);
                $urldb1 = route('requests.show', $requestApproval->request->id);
                $url1   = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2); 
                $user1->notify(new RequestCreated($requestApproval,$url1, $urldb1));
            }
        }
        // stage close
        else if($requestApproval->request->stage_id == Stage::closed()->first()->id)
        {
            $role       = Role::findByName('service desk');
            $collection = $role->users;

            $collection->each(function ($item, $key) use ($requestApproval) {
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
        }
        // stage request rejected 11
        else if($requestApproval->request->stage_id == Stage::requestRejected()->first()->id)
        {
            // notifikasi to service desk
            $role       = Role::findByName('service desk');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
            // notifikasi to manager beict
            $role1          = Role::findByName('manager beict');
            $collection1    = $role1->users;
            $collection1->each(function ($item, $key) use ($requestApproval) {
                $urldb      = route('requests.show', $requestApproval->request->id);
                $url        = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
            // notifikasi to user request
            $user   = User::find($requestApproval->request->user_id);
            $urldb  = route('requests.show', $requestApproval->request->id);
            $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
            $user->notify(new RequestCreated($requestApproval, $url, $urldb));
        }
        // stage request eskalasi so
        else if($requestApproval->request->stage_id == Stage::waitingForSoApproval()->first()->id)
        {
            // cek berdasarkan kategori
            if($requestApproval->request->category_id == 1)
            {
                $role       = Role::where('id', $requestApproval->request->so)->first();
                $user       = $role->users;
                $user->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.bpsshow', [$requestApproval->request->id, $requestApproval->request->service_id, $item->id]),2);
                    $urldb  = route('requests.bpsshow', [$requestApproval->request->id, $requestApproval->request->service_id]);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
            elseif($requestApproval->request->category_id == 2)
            {
                $role       = Role::where('id', $requestApproval->request->so)->first();
                $user       = $role->users;
                $user->each(function ($item, $key) use ($requestApproval) {
                    $urldb  = route('requests.editrecomedation', $requestApproval->request->id);
                    $url    = UrlSigner::sign(route('signed.editrecomedation', [$requestApproval->request->id, $item->id]),2);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
        }
        // stage ticket created 4
        else if($requestApproval->request->stage_id == Stage::ticketCreated()->first()->id)
        {
            if($requestApproval->request->category_id == 1)
            {
                // cek berdasarkan kategori
                $user   = User::find($requestApproval->request->user_id);
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
                $user->notify(new RequestCreated($requestApproval, $url, $urldb));
                
                $role1      = Role::where('id', $requestApproval->request->so)->first();
                $user1      = $role1->users;
                $user1->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.bpsshow', [$requestApproval->request->id, $item->id]),2);
                    $urldb  = route('requests.bpsshow', $requestApproval->request->id);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
            else
            {
                // notifikasi to user request
                $user   = User::find($requestApproval->request->user_id);
                $urldb  = route('requests.show', $requestApproval->request->id);
                $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
                $user->notify(new RequestCreated($requestApproval, $url, $urldb));
            }
        }
        //
        else if($requestApproval->request->stage_id == Stage::waitingForBps()->first()->id)
        {
            if($requestApproval->request->category_id == 1)
            {
                $service_so = RequestSo::where('request_id',$requestApproval->request->id)->get()->toArray();
                $servid     = array_column($service_so, 'service_id');
                $roleid     = array_column(Service::whereIn('id',$servid)->get()->toArray(),'role_id');
                $role1      = Role::whereIn('id', $roleid)->get();
                
                foreach($role1 as $role)
                {
                    $user1          = $role->users;
                    //dd($user1);
                    $user1->each(function ($item, $key) use ($requestApproval, $role) {
                        $service    = array_column(Service::where('role_id',$role->id)->get()->toArray(), 'id');
                        $rso        = RequestSo::whereIn('service_id',$service)->first();
                        //dd($rso->service_id);
                        $url        = UrlSigner::sign(route('signed.bpsshow', [$requestApproval->request->id, $rso->service_id,$item->id]),2);
                        $urldb      = route('requests.bpsshow', [$requestApproval->request->id,$rso->service_id]);
                        $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                    });
                }
            }
        }
        else if($requestApproval->request->stage_id == Stage::waitingForCoordinatorSo()->first()->id)
        {
            $serv        = array_column(RequestSo::where('request_id',$requestApproval->request->id)->get()->toArray(), 'service_id');
            $servsts     = RequestSo::where('request_id',$requestApproval->request->id)->where('status','2')->first();
            //dd($servsts);
            if(!empty($serv))
            {
                if(!isset($servsts))
                {
                    $servco      = array_column(ServiceCoordinator::whereIn('service_id',$serv)->get()->toArray(), 'role_id');
                    $roles       = Role::whereIn('id',$servco)->get();
                    foreach($roles as $role)
                    {
                        $collection = $role->users;
                        //dd($collection);
                        $collection->each(function ($item, $key) use ($requestApproval) {
                            $url    = UrlSigner::sign(route('signed.coshow', [$requestApproval->request->id, $item->id]),2);
                            $urldb  = route('requests.coshow', $requestApproval->request->id);
                            $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                        });
                    }
                }
            }
            else
            {
                $servco      = ServiceCoordinator::where('service_id',$requestApproval->request->service_id)->first()->role_id;
                $roleName    = Role::find($servco)->name;
                $role        = Role::findByName($roleName);
                $collection  = $role->users;
                $collection->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.coshow', [$requestApproval->request->id, $item->id]),2);
                    $urldb  = route('requests.coshow', $requestApproval->request->id);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
        }
        else if($requestApproval->request->stage_id == Stage::waitingForManagerBeict()->first()->id)
        {
            if($requestApproval->request->category_id == 1)
            {
                $role1       = Role::findByName('manager beict');
                $collection1 = $role1->users;
                $collection1->each(function ($item, $key) use ($requestApproval) {
                    $url    = UrlSigner::sign(route('signed.managerbeictshow', [$requestApproval->request->id, $item->id]),2);
                    $urldb  = route('requests.managerbeictshow', $requestApproval->request->id);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
            else
            {
                $role1          = Role::findByName('manager beict');
                $collection1    = $role1->users;
                $collection1->each(function ($item, $key) use ($requestApproval) {
                    $urldb  = route('requests.show', $requestApproval->request->id);
                    $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $item->id]),2);
                    $item->notify(new RequestCreated($requestApproval, $url, $urldb));
                });
            }
        }
        else if($requestApproval->request->stage_id == Stage::transport()->first()->id)
        {
            $user = User::find($requestApproval->request->user_id);
            $boss = $user->boss();
            $urldb1 = route('requests.ndaapproveshow', $requestApproval->request->id);
            $url1   = UrlSigner::sign(route('signed.ndaapproveshow', [$requestApproval->request->id, $boss->id]),2);
            $boss->notify(new RequestCreated($requestApproval,$url1,$urldb1));

            $urldb  = route('requests.show', $requestApproval->request->id);
            $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
            $user1  = User::find($requestApproval->request->user_id);
            $user1->notify(new RequestCreated($requestApproval,$url,$urldb));
        }
        //WaitingForDocument
        else if($requestApproval->request->stage_id == Stage::waitingForDocument()->first()->id)
        {
            $urldb  = route('requests.uploaddocumentshow', $requestApproval->request->id);
            $url    = UrlSigner::sign(route('signed.uploaddocumentshow', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
            $user1  = User::find($requestApproval->request->user_id);
            $user1->notify(new RequestCreated($requestApproval,$url,$urldb));
        }
        else if($requestApproval->request->stage_id == Stage::waitingForSoTransport()->first()->id)
        {
            $role = Role::where('id', $requestApproval->request->so)->first();
            $user = $role->users;
            $user->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.trasportconfirm', [$requestApproval->request->id, $item->id]),2);
                $urldb  = route('requests.trasportconfirm', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
        }
        else if($requestApproval->request->stage_id == Stage::level2()->first()->id)
        {
            $urldb  = route('requests.show', $requestApproval->request->id);
            $url    = UrlSigner::sign(route('signed.show', [$requestApproval->request->id, $requestApproval->request->user_id]),2);
            $user1  = User::find($requestApproval->request->user_id);
            $user1->notify(new RequestCreated($requestApproval,$url,$urldb));
        }
        else if($requestApproval->request->stage_id == Stage::transportSuccess()->first()->id)
        {
            $role       = Role::findByName('service desk');
            $collection = $role->users;
            $collection->each(function ($item, $key) use ($requestApproval) {
                $url    = UrlSigner::sign(route('signed.detailcrfshow', [$requestApproval->request->id, $item->id]),2);
                $urldb  = route('requests.detailcrfshow', $requestApproval->request->id);
                $item->notify(new RequestCreated($requestApproval, $url, $urldb));
            });
        }

    }

    /**
     * Handle the request approval "updated" event.
     *
     * @param  \App\RequestApproval  $requestApproval
     * @return void
     */
    public function updated(RequestApproval $requestApproval)
    {
        
    }

    /**
     * Handle the request approval "deleted" event.
     *
     * @param  \App\RequestApproval  $requestApproval
     * @return void
     */
    public function deleted(RequestApproval $requestApproval)
    {
        //
    }

    /**
     * Handle the request approval "restored" event.
     *
     * @param  \App\RequestApproval  $requestApproval
     * @return void
     */
    public function restored(RequestApproval $requestApproval)
    {
        //
    }

    /**
     * Handle the request approval "force deleted" event.
     *
     * @param  \App\RequestApproval  $requestApproval
     * @return void
     */
    public function forceDeleted(RequestApproval $requestApproval)
    {
        //
    }
}
