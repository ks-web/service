<?php

namespace App\Observers;

use App\RequestChange;
use App\Status;
use App\User;
use App\Stage;
use App\Action;
use App\Role;
use App\RequestChangeAction;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RequestChangeObserver
{
    /**
     * Handle the request "created" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function created(RequestChange $requestchange)
    {
        $rchange =  RequestChangeAction::create([
            'request_change_id' => $requestchange->id,
            'user_id'           => Auth::user()->id,
            'status_id'         => Status::waitingForApproval()->first()->id,
            'action_type'       => Action::Ccreate()->first()->id,
            'stage_id'          => Stage::waitingBossApproval()->first()->id,
            'role_id'           => Role::roleEmployee()->first()->id
        ]);

    }

    /**
     * Handle the request "updated" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function updated(RequestChange $requestchange, Request $request)
    {
       
    }

    /**
     * Handle the request "deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function deleted(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "restored" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function restored(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "force deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function forceDeleted(ITRequest $request)
    {
        //
    }
}
