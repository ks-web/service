<?php

namespace App\Observers;

use App\Difficulty;
use App\Priority;
use App\RequestChangeBps;
use App\RequestChange;
use App\Stage;
use App\Action;
use App\Status;
use App\RequestChangeBpsDuration;
use App\RequestChangeBpsAction;
use App\ServiceCoordinator;
use App\RequestChangeAction;
use App\RequestChangeBpsActionNote;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RequestChangeBpsObserver
{
    /**
     * Handle the request "created" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function created(RequestChangeBps $requestchangeBps)
    {
        $rchange =  RequestChangeBpsAction::create([
            'request_change_bps_id' => $requestchangeBps->id,
            'user_id'           => Auth::user()->id,
            'status_id'         => Status::approved()->first()->id,
            'action_type'       => Action::uploadBps()->first()->id,
            'stage_id'          => Stage::waitingBossApproval()->first()->id
        ]);

    }

    /**
     * Handle the request "updated" event.
     *
     * @param  \App\RequestChange  $request
     * @return void
     */
    public function updated(RequestChange $requestchange, Request $request)
    {
       
    }

    /**
     * Handle the request "deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function deleted(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "restored" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function restored(ITRequest $request)
    {
        //
    }

    /**
     * Handle the request "force deleted" event.
     *
     * @param  \App\ITRequest  $request
     * @return void
     */
    public function forceDeleted(ITRequest $request)
    {
        //
    }
}
