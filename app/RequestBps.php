<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestBps extends Model
{
    public function request()
    {
        return $this->belongsTo('App\ITRequest');
    }
}
