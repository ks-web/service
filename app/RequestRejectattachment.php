<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestRejectattachment extends Model
{
    protected $fillable = ['attachment','name','alias'];

    public function request()
    {
        return $this->belongsTo('App\ITRequest');
    }
}
