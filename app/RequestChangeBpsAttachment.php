<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeBpsAttachment extends Model
{
    //
    protected $fillable = ['request_change_bps_id','attachment','name','alias'];

    // relation with request
    public function requestChangeBps()
    {
        return $this->belongsTo('App\RequestChangeBps');
    }
}
