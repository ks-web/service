<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $fillable = ['name'];

    public function requests()
    {
        return $this->hasMany('App\ITRequest');
    }

    public function requestChanges()
    {
        return $this->hasMany('App\RequestChange');
    }

    public function RequestChangeBps()
    {
        return $this->hasMany('App\RequestChangeBps');
    }

    public function incidents()
    {
        return $this->hasMany('App\Incident');
    }

    public function requestApprovals()
    {
        return $this->hasMany('App\RequestApproval', 'stage_id');
    }
    
    public function scopeWaitingBossApproval($query)
    {
        $query->where('id', 1);
    }

    public function scopeWaitingForOperationDesk($query)
    {
        $query->where('id', 2);
    }

    public function scopeWaitingForServiceDesk($query)
    {
        $query->where('id', 3);
    }

    public function scopeTicketCreated($query)
    {
        $query->where('id', 4);
    }

    public function scopeResolve($query)
    {
        $query->where('id', 5);
    }

    public function scopeWaitingUserConf($query)
    {
        $query->where('id', 6);
    }

    public function scopeWaitingForOperationIct($query)
    {
        $query->where('id', 7);
    }

    public function scopeRequestDenied($query)
    {
        $query->where('id', 8);
    }

    public function scopeClosed($query)
    {
        $query->where('id', 9);
    }

    public function scopeWaitingForSoApproval($query)
    {
        $query->where('id', 10);
    }

    public function scopeRequestRejected($query)
    {
        $query->where('id', 11);
    }
    
    public function scopeWaitingForManagerBeict($query)
    {
        $query->where('id', 12);
    }

    public function scopeWaitingForCoordinatorSo($query)
    {
        $query->where('id', 13);
    }

    public function scopeWaitingForBps($query)
    {
        $query->where('id', 14);
    }

    public function scopeWaitingForSoRelease($query)
    {
        $query->where('id', 15);
    }

    public function scopeTransport($query)
    {
        $query->where('id', 16);
    }

    public function scopeTransportSuccess($query)
    {
        $query->where('id', 17);
    }

    public function scopeWaitingForSoTransport($query)
    {
        $query->where('id', 18);
    }

    public function scopeWaitingForChiefApproval($query)
    {
        $query->where('id', 19);
    }

    public function scopeWaitingForInformationDetail($query)
    {
        $query->where('id', 19);
    }

    public function scopeApproved($query)
    {
        $query->where('id', 21);
    }

    public function scopeWaitingForDocument($query)
    {
        $query->where('id', 28);
    }

    public function scopeDeniedByUser($query)
    {
        $query->where('id', 29);
    }

    public function scopeLevel2($query)
    {
        $query->where('id', 30);
    }

    public function scopeArsip($query)
    {
        $query->where('id', 31);
    }

    public function scopeFailedStage($query)
    {
        $query->where('id', 38);
    }

    public function scopeApproveKaseya($query)
    {
        $query->where('id', 39);
    }

    public function scopeUpdateBps($query)
    {
        $query->where('id', 40);
    }
}
