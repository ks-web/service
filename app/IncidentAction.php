<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentAction extends Model
{
    protected $fillable = ['user','action_type'];

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_type');
    }

    public function users()
    {
        return $this->belongsTo('App\User', 'user');
    }

    public function incident()
    {
        return $this->belongsTo('App\Incident');
    }

    public function incidentActionNotes()
    {
        return $this->hasOne('App\IncidentActionNote');
    }
}
