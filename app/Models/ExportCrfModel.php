<?php

namespace App\Models;

use App\Action;
use App\Difficulty;
use App\Priority;
use App\RequestBps;
use App\RequestChangeBps;
use App\Service;
use App\Stage;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ExportCrfModel extends Model
{
    protected $table = 'request_changes';

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }

    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public function priority()
    {
        return $this->belongsTo(Priority::class);
    }

    public function difficulty()
    {
        return $this->belongsTo(Difficulty::class);
    }
}
