<?php

namespace App\Models;

use App\Service;
use App\Stage;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ExportSrfModel extends Model
{

    protected $table = 'requests';

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stage()
    {
        return $this->belongsTo(Stage::class);
    }
}
