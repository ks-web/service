<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExportIncidentsModel extends Model
{
    protected $table = 'incidents';
    protected $guarded = ['id'];

    public function priority()
    {
        return $this->belongsTo('App\Priority');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }
}
