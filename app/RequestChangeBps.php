<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RequestChangeBps extends Model
{
    //
    protected $fillable = ['request_change_id', 'priority_id', 'difficulty_id', 'service_id', 'stage_id', 'status_id', 'preparedby', 'module', 'email', 'phone', 'ticket_no', 'bpj', 'global_design', 'detail_specification', 'effort', 'start_plan_date', 'finish_plan_date', 'finish_date', 'so_approve_name', 'so_approve_status', 'so_approve_date', 'co_approve_name', 'co_approve_status', 'co_approve_date'];

    public function requestChange()
    {
        return $this->belongsTo('App\RequestChange');
    }

    public function requestChangeBpsActions()
    {
        return $this->hasMany('App\RequestChangeBpsAction');
    }

    // relation with RequestChangeAttachments
    public function requestChangeBpsAttachments()
    {
        return $this->hasMany('App\RequestChangeBpsAttachment', 'request_change_bps_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    public function stage()
    {
        return $this->belongsTo('App\Stage');
    }

    public function priority()
    {
        return $this->belongsTo('App\Priority');
    }

    public function difficulty()
    {
        return $this->belongsTo('App\Difficulty');
    }

    public function requestChangeBpsDurations()
    {
        return $this->hasMany('App\RequestChangeBpsDuration');
    }

    public function scopeOfLoggedUser($query)
    {
        $query->where('user_id', Auth::user()->id);
    }

    public function scopeOfBossSubordinates($query)
    {
        $query->whereIn('user_id', Auth::user()->getPersonnelNoSubordinates());
    }
}
