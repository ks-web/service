<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeRejectAttachment extends Model
{
    protected $fillable = ['request_change_id','attachment','name','alias'];

    // relation with request
    public function requestChange()
    {
        return $this->belongsTo('App\RequestChange');
    }
}
