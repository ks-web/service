<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeBpsAction extends Model
{
    protected $fillable = ['request_change_bps_id','user_id','action_type','status_id','stage_id'];
    
    public function requestChangeBps()
    {
        return $this->belongsTo('App\RequestChangeBps');
    }

    public function requestChangeBpsActionNotes()
    {
        return $this->hasOne('App\RequestChangeBpsActionNote');
    }

    public function action()
    {
        return $this->belongsTo('App\Action', 'action_type')->orderBy('id','DESC');;
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
