<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncidentActionNote extends Model
{
    protected $fillable = ['note'];

    public function incidentAction()
    {
        return $this->belongsTo('App\IncidentAction');
    }
}
