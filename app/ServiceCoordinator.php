<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCoordinator extends Model
{
    protected $fillable = ['name'];

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function rolenya()
    {
        return $this->belongsTo('Spatie\Permission\Models\Role','role_id','id');
    }
}
