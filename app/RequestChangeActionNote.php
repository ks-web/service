<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestChangeActionNote extends Model
{
    protected $fillable = ['note'];

    public function requestChangeAction()
    {
        return $this->belongsTo('App\RequesChangetAction');
    }
}
