<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = ['name'];

    public function requestActions()
    {
        return $this->hasMany('App\RequestAction');
    }

    public function incidentActions()
    {
        return $this->hasMany('App\IncidentAction');
    }

    public function requestChanges()
    {
        return $this->hasMany('App\RequestChange');
    }

    public function requestChangeActions()
    {
        return $this->hasMany('App\RequestChangeAction');
    }

    public function requestChangeBpsActions()
    {
        return $this->hasMany('App\RequestChangeBpsAction');
    }

    public function scopeCcreate($query)
    {
        $query->where('id', 1);
    }

    public function scopeApprove($query)
    {
        $query->where('id', 2);
    }

    public function scopeReject($query)
    {
        $query->where('id', 3);
    }

    public function scopeEscalation($query)
    {
        $query->where('id', 4);
    }

    public function scopeConfirmationTransport($query)
    {
        $query->where('id', 5);
    }

    public function scopeTicketInput($query)
    {
        $query->where('id', 6);
    }

    public function scopeDetailInput($query)
    {
        $query->where('id', 7);
    }

    public function scopeNdaApprove($query)
    {
        $query->where('id', 8);
    }

    public function scopeTransport($query)
    {
        $query->where('id', 9);
    }

    public function scopeBpsRelease($query)
    {
        $query->where('id', 10);
    }

    public function scopeUploadBps($query)
    {
        $query->where('id', 11);
    }

    public function scopeRecomendation($query)
    {
        $query->where('id', 12);
    }

    public function scopeNotRecomendation($query)
    {
        $query->where('id', 13);
    }

    public function scopeWaitingDocument($query)
    {
        $query->where('id', 14);
    }

    public function scopeChooseSo($query)
    {
        $query->where('id', 15);
    }

    public function scopeUnrelease($query)
    {
        $query->where('id', 16);
    }

    public function scopeUploadFile($query)
    {
        $query->where('id', 17);
    }

    public function scopeInputNote($query)
    {
        $query->where('id', 18);
    }

    public function scopeArsip($query)
    {
        $query->where('id', 19);
    }
    
    public function scopeSystemProcess($query)
    {
        $query->where('id', 20);
    }

    public function scopeFailAction($query)
    {
        $query->where('id', 21);
    }
}
