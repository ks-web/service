<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestActionNote extends Model
{
    protected $fillable = ['note'];

    public function requestAction()
    {
        return $this->belongsTo('App\RequestAction');
    }
}
