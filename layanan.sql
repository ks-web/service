-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2019 at 03:49 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `layanan`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'create', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(2, 'approve', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(3, 'reject', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(4, 'escalation', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(5, 'confirmation transport', '2019-02-17 09:39:43', '2019-02-17 09:39:43'),
(6, 'ticket input', '2019-02-17 09:41:56', '2019-02-17 09:41:56'),
(7, 'detail input', '2019-02-17 21:31:26', '2019-02-17 21:31:26'),
(8, 'nda aprove', '2019-02-18 07:44:45', '2019-02-18 07:44:45'),
(9, 'transport', '2019-02-18 15:30:47', '2019-02-18 15:30:47'),
(10, 'release', '2019-02-18 15:39:09', '2019-02-18 15:39:09'),
(11, 'upload bps', '2019-02-18 15:39:41', '2019-02-18 15:39:41'),
(12, 'recomendation', '2019-02-18 15:40:13', '2019-02-18 15:40:13'),
(13, '', '2019-02-18 15:45:50', '2019-02-18 15:45:50'),
(14, '', '2019-02-18 15:48:20', '2019-02-18 15:48:20'),
(15, '', '2019-02-18 15:49:10', '2019-02-18 15:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'perubahan atau penambahan aplikasi', '2019-02-03 04:24:37', '2019-02-03 04:24:42'),
(2, 'permintaan akses/layanan', '2019-02-03 04:25:27', '2019-02-03 04:25:32');

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `impact` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `ticket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incident_actions`
--

CREATE TABLE `incident_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `incident_id` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `action_type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incident_approvals`
--

CREATE TABLE `incident_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `incident_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2014_10_12_100000_create_password_resets_table', 1),
(14, '2019_01_18_075248_create_layanan_tables', 1),
(15, '2019_01_21_090953_create_permission_tables', 1),
(16, '2019_01_22_075346_create_notifications_table', 1),
(17, '2019_02_14_015210_create_actions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 4),
(1, 'App\\User', 5),
(1, 'App\\User', 6),
(1, 'App\\User', 6833),
(1, 'App\\User', 10112),
(1, 'App\\User', 10113),
(1, 'App\\User', 10114),
(1, 'App\\User', 10115),
(1, 'App\\User', 10116),
(1, 'App\\User', 10117),
(1, 'App\\User', 10696),
(1, 'App\\User', 10709),
(1, 'App\\User', 11420),
(2, 'App\\User', 3),
(3, 'App\\User', 10112),
(3, 'App\\User', 10696),
(3, 'App\\User', 10709),
(3, 'App\\User', 11420),
(4, 'App\\User', 5),
(5, 'App\\User', 10115),
(6, 'App\\User', 4),
(10, 'App\\User', 6),
(17, 'App\\User', 10112),
(18, 'App\\User', 10113),
(19, 'App\\User', 10114),
(20, 'App\\User', 10116),
(21, 'App\\User', 10117);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('08e0cc3a-f175-4c97-988a-f2804d0c144e', 'App\\Notifications\\RequestCreated', 'App\\User', 6833, '{\"stage_id\":3,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/show\"}', NULL, '2019-03-05 19:15:14', '2019-03-05 19:15:14'),
('166b4a77-150f-4bb2-af24-aa08a0b7926f', 'App\\Notifications\\RequestCreated', 'App\\User', 10112, '{\"stage_id\":6,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', NULL, '2019-03-04 02:50:42', '2019-03-04 02:50:42'),
('1681ecfe-6247-4a8b-959d-47f4f0da9d93', 'App\\Notifications\\RequestCreated', 'App\\User', 6, '{\"stage_id\":2,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/show\"}', NULL, '2019-03-05 19:01:15', '2019-03-05 19:01:15'),
('271bf32c-6c2b-42c2-94ab-a48d659cb3c2', 'App\\Notifications\\RequestCreated', 'App\\User', 10112, '{\"stage_id\":1,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/approveshow\"}', '2019-03-05 19:01:15', '2019-03-04 02:06:44', '2019-03-05 19:01:15'),
('333fc865-eb48-47a3-a45d-29430acc4570', 'App\\Notifications\\RequestCreated', 'App\\User', 6833, '{\"stage_id\":6,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', '2019-03-04 02:52:29', '2019-03-04 02:50:47', '2019-03-04 02:52:29'),
('3ed6ec03-73f6-4c63-a8ef-f8bc6efcfb16', 'App\\Notifications\\RequestCreated', 'App\\User', 5, '{\"stage_id\":2,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/spsd\\/show\"}', '2019-03-04 02:12:33', '2019-03-04 01:58:17', '2019-03-04 02:12:33'),
('49f4b9a2-06a0-4268-9003-c1a6b09cfea9', 'App\\Notifications\\RequestCreated', 'App\\User', 3, '{\"stage_id\":3,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/edit\\/ticket\"}', '2019-03-04 02:29:23', '2019-03-04 02:20:22', '2019-03-04 02:29:23'),
('50c4ca86-171e-4f8a-bc4a-e0838075199f', 'App\\Notifications\\RequestCreated', 'App\\User', 10112, '{\"stage_id\":1,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/approveshow\"}', '2019-03-04 01:58:18', '2019-03-04 01:08:02', '2019-03-04 01:58:18'),
('5cc93ea5-2200-4e25-bc36-60358cf718d8', 'App\\Notifications\\RequestCreated', 'App\\User', 10112, '{\"stage_id\":2,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', NULL, '2019-03-04 01:58:17', '2019-03-04 01:58:17'),
('90f19901-547c-4572-b69a-338b19719ede', 'App\\Notifications\\RequestCreated', 'App\\User', 6833, '{\"stage_id\":3,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', NULL, '2019-03-04 02:20:21', '2019-03-04 02:20:21'),
('9ece8441-534a-4fa8-93c9-963f7f851c93', 'App\\Notifications\\RequestCreated', 'App\\User', 6833, '{\"stage_id\":4,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', NULL, '2019-03-04 02:32:35', '2019-03-04 02:32:35'),
('a68f660b-230a-43d9-b41b-08355f6bdac5', 'App\\Notifications\\RequestCreated', 'App\\User', 3, '{\"stage_id\":3,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/edit\\/ticket\"}', NULL, '2019-03-05 19:15:14', '2019-03-05 19:15:14'),
('a7c5f730-90f7-45ed-b9bb-32a51ac0b615', 'App\\Notifications\\RequestCreated', 'App\\User', 6, '{\"stage_id\":2,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', NULL, '2019-03-04 01:58:17', '2019-03-04 01:58:17'),
('c7dfd727-5990-47db-bc79-2e0685f61c93', 'App\\Notifications\\RequestCreated', 'App\\User', 5, '{\"stage_id\":2,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/spsd\\/show\"}', '2019-03-05 19:15:14', '2019-03-05 19:01:09', '2019-03-05 19:15:14'),
('d4747efc-bbc1-48e0-9e7a-8eef0c4735ed', 'App\\Notifications\\RequestCreated', 'App\\User', 3, '{\"stage_id\":9,\"id\":1,\"business_benefit\":\"Menggantikan persnonel yang pensiun\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/1\\/show\"}', '2019-03-04 02:52:44', '2019-03-04 02:52:26', '2019-03-04 02:52:44'),
('ff03818d-6213-4c24-97bb-c34cc14d51e5', 'App\\Notifications\\RequestCreated', 'App\\User', 10112, '{\"stage_id\":2,\"id\":2,\"business_benefit\":\"Tes alasan\",\"url\":\"http:\\/\\/127.0.0.1:200\\/requests\\/2\\/show\"}', NULL, '2019-03-05 19:01:15', '2019-03-05 19:01:15');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'browse request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(2, 'create request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(3, 'edit request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(4, 'delete request', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(5, 'browse incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(6, 'create incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(7, 'edit incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(8, 'delete incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(9, 'employee incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(10, 'crud stages', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(11, 'crud statuses', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(12, 'crud services', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(13, 'boss approval', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(14, 'operation approval', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02'),
(15, 'servicedesk update', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02'),
(16, 'so approval', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_need` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_benefit` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `ticket` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `nda` int(1) DEFAULT NULL,
  `nda_date` timestamp NULL DEFAULT NULL,
  `so` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recomendation_status` int(1) DEFAULT NULL,
  `crf_category` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `service_id`, `title`, `business_need`, `business_benefit`, `attachment`, `detail`, `ticket`, `category_id`, `reason`, `user_id`, `stage_id`, `keterangan`, `description`, `nda`, `nda_date`, `so`, `recomendation_status`, `crf_category`, `created_at`, `updated_at`) VALUES
(1, 3, 'Akses aplikasi uang muka', 'Menggantikan persnonel yang pensiun', 'Menunjang pekerjaan', NULL, 'Sudah di kerjakan sesuai permintaan', 'IN2000', 2, NULL, 6833, 9, 'Nik: 101010', NULL, 1, '2019-03-04 01:07:58', '6', NULL, 0, '2019-03-04 01:07:58', '2019-03-04 02:52:25'),
(2, 3, 'Akses aplikasi reservasi', 'Tes alasan', 'Tes manfaat bisnis', NULL, NULL, NULL, 2, NULL, 6833, 3, 'Tes keterangan tambahan', NULL, 1, '2019-03-04 02:06:44', '6', NULL, 0, '2019-03-04 02:06:44', '2019-03-05 19:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `request_actions`
--

CREATE TABLE `request_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `action_type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_actions`
--

INSERT INTO `request_actions` (`id`, `request_id`, `user`, `action_type`, `created_at`, `updated_at`) VALUES
(1, 1, 6833, 1, '2019-03-04 01:08:11', '2019-03-04 01:08:11'),
(2, 1, 10112, 2, '2019-03-04 01:58:18', '2019-03-04 01:58:18'),
(3, 2, 6833, 1, '2019-03-04 02:06:45', '2019-03-04 02:06:45'),
(4, 1, 5, 2, '2019-03-04 02:20:22', '2019-03-04 02:20:22'),
(5, 1, 3, 6, '2019-03-04 02:32:36', '2019-03-04 02:32:36'),
(6, 1, 3, 7, '2019-03-04 02:50:47', '2019-03-04 02:50:47'),
(7, 1, 6833, 2, '2019-03-04 02:52:29', '2019-03-04 02:52:29'),
(8, 2, 10112, 2, '2019-03-05 19:01:15', '2019-03-05 19:01:15'),
(9, 2, 5, 2, '2019-03-05 19:15:14', '2019-03-05 19:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `request_action_notes`
--

CREATE TABLE `request_action_notes` (
  `id` int(11) NOT NULL,
  `request_action_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_action_notes`
--

INSERT INTO `request_action_notes` (`id`, `request_action_id`, `note`, `created_at`, `updated_at`) VALUES
(1, 2, 'Oke', '2019-03-04 01:58:18', '2019-03-04 01:58:18'),
(2, 4, 'Sudah sesuai WI', '2019-03-04 02:20:22', '2019-03-04 02:20:22'),
(3, 8, 'Tes note', '2019-03-05 19:01:15', '2019-03-05 19:01:15'),
(4, 9, 'Ok lanjut', '2019-03-05 19:15:15', '2019-03-05 19:15:15');

-- --------------------------------------------------------

--
-- Table structure for table `request_approvals`
--

CREATE TABLE `request_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `request_approvals`
--

INSERT INTO `request_approvals` (`id`, `request_id`, `user_id`, `status_id`, `stage_id`, `created_at`, `updated_at`) VALUES
(1, 1, 6833, 1, 1, '2019-03-04 01:07:58', '2019-03-04 01:07:58'),
(2, 1, 10112, 2, 2, '2019-03-04 01:58:17', '2019-03-04 01:58:17'),
(3, 2, 6833, 1, 1, '2019-03-04 02:06:44', '2019-03-04 02:06:44'),
(4, 1, 5, 2, 3, '2019-03-04 02:20:21', '2019-03-04 02:20:21'),
(5, 1, 3, 2, 4, '2019-03-04 02:32:35', '2019-03-04 02:32:35'),
(6, 1, 3, 2, 6, '2019-03-04 02:50:40', '2019-03-04 02:50:40'),
(7, 1, 6833, 2, 9, '2019-03-04 02:52:25', '2019-03-04 02:52:25'),
(8, 2, 10112, 2, 2, '2019-03-05 19:01:06', '2019-03-05 19:01:06'),
(9, 2, 5, 2, 3, '2019-03-05 19:15:13', '2019-03-05 19:15:13');

-- --------------------------------------------------------

--
-- Table structure for table `request_attachments`
--

CREATE TABLE `request_attachments` (
  `id` int(10) NOT NULL,
  `request_id` int(10) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `request_attachments`
--

INSERT INTO `request_attachments` (`id`, `request_id`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 1, 'attachments/DipiNH5lgoGRAhJ5XqrA8jfAJbTATxGtGplHb1Kr.jpeg', '2019-03-04 01:08:11', '2019-03-04 01:08:11'),
(2, 1, 'attachments/A0pWBvQrgPYvU1HZJHJuAApwtTGab1qVbrl4fv80.jpeg', '2019-03-04 01:08:11', '2019-03-04 01:08:11'),
(3, 2, 'attachments/YhP2HLG1F2PjrVoeVFgTQ8d9WuvfMQ6Avh5l5Nhj.jpeg', '2019-03-04 02:06:45', '2019-03-04 02:06:45'),
(4, 2, 'attachments/MAEenqNkvjHegyggt3kvha1XZn4ku0t9t2BGtciu.jpeg', '2019-03-04 02:06:45', '2019-03-04 02:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `request_reasonfiles`
--

CREATE TABLE `request_reasonfiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) DEFAULT NULL,
  `attachment` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_sos`
--

CREATE TABLE `request_sos` (
  `id` int(10) NOT NULL,
  `request_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_sos`
--

INSERT INTO `request_sos` (`id`, `request_id`, `service_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 0, '2019-03-04 01:08:11', '2019-03-04 01:08:11'),
(2, 2, 3, 0, '2019-03-04 02:06:45', '2019-03-04 02:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `request_so_bps`
--

CREATE TABLE `request_so_bps` (
  `id` int(11) NOT NULL,
  `request_sos_id` int(10) NOT NULL,
  `attachment` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'employee', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(2, 'service desk', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(3, 'boss', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(4, 'operation sd', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(5, 'so mes flat', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(6, 'so web', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(7, 'so mes long', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(8, 'so sap hr', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(9, 'so sap pp', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(10, 'operation ict', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(11, 'so sap fi', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(12, 'so sap co', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(13, 'so sap sd', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(14, 'so mes im', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(15, 'so mes mm', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(16, 'so sap ps', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(17, 'manager beict', 'web', '2019-02-01 03:46:56', '2019-02-01 03:47:02'),
(18, 'coordinator aplikasi', 'web', '2019-02-03 10:12:07', '2019-02-03 10:12:11'),
(19, 'coordinator infra', 'web', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(20, 'so perangkat komputer', 'web', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(21, 'chief', 'web', '2019-02-11 17:00:00', '2019-02-12 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 17),
(2, 1),
(2, 17),
(3, 1),
(3, 17),
(4, 1),
(4, 17),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 2),
(11, 2),
(12, 2),
(13, 3),
(14, 4),
(14, 10),
(15, 2),
(16, 5),
(16, 6),
(16, 7),
(16, 8),
(16, 9),
(16, 11),
(16, 12),
(16, 13),
(16, 14),
(16, 16);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `groupserv` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `groupserv`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Layanan aplikasi sap MM', 'sap', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'Layanan aplikasi mes long', 'mes', 7, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'Layanan aplikasi web', '', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(4, 'Layanan aplikasi Office', '', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(5, 'Layanan messaging', '', 6, '2019-02-01 06:31:48', '2019-02-01 06:31:54'),
(6, 'Layanan perangkat komputer', '', 20, '2019-02-01 06:32:29', '2019-02-01 06:32:33'),
(7, 'Layanan printer', '', 6, '2019-02-01 06:32:58', '2019-02-01 06:33:03'),
(8, 'Layanan Jaringan', '', 6, '2019-02-01 06:33:26', '2019-02-01 06:33:30'),
(9, 'Layanan video conference', '', 6, '2019-02-01 06:34:15', '2019-02-01 06:34:22'),
(10, 'Layanan aplikasi mes flat', 'mes', 5, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(11, 'Layanan aplikasi sap PPQM', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(12, 'Layanan aplikasi sap FICO', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(13, 'Layanan aplikasi sap SD', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(14, 'Layanan aplikasi sap HR', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `service_coordinators`
--

CREATE TABLE `service_coordinators` (
  `id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_coordinators`
--

INSERT INTO `service_coordinators` (`id`, `role_id`, `service_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 18, 1, 'Aplikasi', '2019-02-12 08:13:32', '2019-02-11 17:00:00'),
(2, 18, 2, 'Aplikasi', '2019-02-12 08:16:26', '2019-02-11 17:00:00'),
(3, 18, 3, 'Aplikasi', '2019-02-12 08:16:26', '2019-02-11 17:00:00'),
(4, 18, 10, 'aplikasi', '2019-02-16 14:05:25', '0000-00-00 00:00:00'),
(5, 19, 6, 'infra', '2019-02-17 10:22:42', '2019-02-16 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'waiting for Mgr/GM approval', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'waiting for operation desk', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'waiting for service desk', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(4, 'ticket created', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(5, 'resolved', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(6, 'waiting user confirmation', '2019-01-29 07:27:28', '2019-01-29 07:27:33'),
(7, 'waiting for operation ict', '2019-01-30 10:05:49', '2019-01-30 10:05:54'),
(8, 'request denied', '2019-01-30 10:35:37', '2019-01-30 10:35:43'),
(9, 'closed', '2019-01-30 10:49:03', '2019-01-30 10:49:12'),
(10, 'waiting for so approval', '2019-01-23 13:57:15', '2019-01-30 13:57:20'),
(11, 'request rejected', '2019-02-01 04:13:35', '2019-02-08 04:13:40'),
(12, 'waiting for Manager BEICT', '2019-02-10 17:00:00', '2019-02-10 17:00:00'),
(13, 'waiting for coordinator so approval', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(14, 'waiting for bps from other service owners\r\n', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(15, 'waiting for so release', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(16, 'waiting for boss approval for transport', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(17, 'Transport success', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(18, 'waiting for so transport', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(19, 'waiting for chief approval', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(20, 'waiting for detail information', '2019-02-17 17:00:00', '2019-02-17 17:00:00'),
(21, 'Approved', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(22, 'Change Failed', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(23, 'Change Successful', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(24, 'Denied', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(25, 'Implemented', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(26, 'Preparation', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(27, 'Recorded', '2019-02-27 17:00:00', '2019-02-27 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'waiting for approval', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'approved', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'rejected', '2019-01-23 00:33:31', '2019-01-23 00:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'ITA NURJANAH', 'sd@fmail.com', '$2y$10$8NeU0ilffIdzD0UsYGuPzeKoCefm9gUXxpwF/uq0x3w352TnyMkO2', 'RqOHYy6rcSr38G3MZ3tBspt2OiYdCNThc6G6sVNfRLCRTNCfKG7QRVglL8jj', '2019-01-23 00:33:32', '2019-01-23 00:33:32'),
(4, 'RAHMADHANY TRIASTANTO', 'so@fmail.com', '$2y$10$1ulV41HzatLGU8uZ7t.7bOfBC4fGyVccUpTSeyV7aMYYnBLdqeBnO', 'hxrg5NbvNzZmvJliqk6xan4x6yeRJrvm1dz74mTlAvoVDrEv0P54eIlswH0b', '2019-01-23 00:33:32', '2019-01-23 00:33:32'),
(5, 'ANDI SOFIA KARINA', 'spsd@fmail.com', '$2y$10$xvhQgX5JDIBA5t3SSpmWbOodbYz6xliIlHMFkgBVuAMb6/bXCfYP.', 'JzGRvYSlufPLk1ql0yRVrxC7NcNa5o6Ehbh4EXk4LJDoKvWfgW1J4ZDVbvvM', '2019-01-23 00:33:33', '2019-01-23 00:33:33'),
(6, 'OTTE DA', 'spict@fmail.com', '$2y$10$ecD/iKvERilg1v.edwBKJO9HQLHzu2EcJLb79AWtMMUrHGcZuFH0O', '6FgYHQYlLH7XuLSQGcpKqYL9o0kBynp0RDWLnN6bids9Mv1nANyqGJ89kfJJ', '2019-01-23 00:33:33', '2019-01-23 00:33:33'),
(6833, 'UJANG FURKON', 'user@test.com', '$2y$10$W0lJHrCa4U45Gf0Mnnww9OnrBYZFTQyBwTisx..VMeIb24e1txyRK', '8GYAv9ZAOUvEMiF5CFj6GNqEiQyP5dU2K819OmsJyf75kPMmKRfZqsnn8XBZ', '2019-01-23 00:33:32', '2019-01-25 00:29:32'),
(10112, 'MUHAMMAD HELMI NUR WIDIYARTO', 'el@fmail.com', '$2y$10$siWBJZrI4NfMPVnMq1ZaEOmbChtgrem25ecoK8MnWyAXrw6hdhHsq', 'ZWc9j8VoixCTDBtwxdokDzkp2CoAfbYAr2u7Cpj79rzVmoraGgobdw6pr93k', '2019-01-23 00:33:32', '2019-01-23 19:02:56'),
(10113, 'coorditator apilkasi', 'coaplikasi@fmail.com', '$2y$10$1LZGE8Tw5s4QIbfYJWMbRedHL6ad/YgijGMLAfu3FSpQUDvQUPEPW', 'Lg87dWVIUhOcyAneOWFYWDJsdm820ppmy7DEpcPuBiDLKkdUEfBJZkGPOjfa', '2019-02-11 21:53:10', '2019-02-11 21:53:10'),
(10114, 'coinfra', 'coinfra@fmail.com', '$2y$10$PldJEdJLq5poNzun7sLFk.iWrn80AHY4ic0bcCwT6yV.ATmg0NfEC', 'wAGcbkLbGS0aXGXenKPxALCjRKkOp0pilAQW9fiMQVxEwb41l6GqlzUkFWXM', '2019-02-11 21:54:26', '2019-02-11 21:54:26'),
(10115, 'Husni Mubarok', 'somesflat@fmail.com', '$2y$10$lkwssFhREaDojw8Vm84IMu3QUTcodV0E3agYVDV5TZaKy4YVT1ZnC', 'JPuDnCYhZU61qBguVPdBzYJ9nqFljYsjmRNTMlMmU2nwbX0iEuicFI949ocD', '2019-02-13 15:24:00', '2019-02-16 05:15:46'),
(10116, 'so perangkat komputer', 'sopk@fmail.com', '$2y$10$Vdd5zOll2ZAUiVGoUC3zYeRJ3Rgr2nAU2mJ8LQClSU1JZy21r8J6a', '3JGN35fodEY4drAnSx5ax6X3NSJCiFHif1DnUahwfCKQr4f71M1NIJYNuQWR', '2019-02-17 03:39:56', '2019-02-17 03:39:56'),
(10117, 'chief', 'chief@fmail.com', '$2y$10$kx3lEmOVh1vt5qHrdyjjHOn1MbtQ3MNWyRGIpw.D/GIdy/ZTY.o3m', 'k2uAAjpfi4WgM7lS2ycSQ2UMDJdfzM2P0ne0DMBhtCpCn0btHPJKbNceHYSg', '2019-02-17 08:31:44', '2019-02-17 08:31:44'),
(10696, 'WENI PURWANINGRUM', '', '$2y$10$bx9Yl1hIBg0He7BaP./V/ORiW0eYOEVPOqJ6mvxuItzp4MrH2NIQq', NULL, '2019-02-21 00:52:21', '2019-02-21 00:52:21'),
(10709, 'DICKY MARDIANA', 'dicky@fmail.com', '$2y$10$iPyL0Hhd2M2yJpkhr3N8R.gK9cyIpXBjTx0NR2v/2BWj/AnGB0ieq', NULL, '2019-02-21 01:15:54', '2019-02-21 01:15:54'),
(11420, 'JOKO RUSANDI AZHARI', 'joko@fmail.com', '$2y$10$SapA4crNWwG9t2rd2exWIutXCLlI.GPyAOxmx8.NI6qBbd0/q3Iqy', 'W4IFMWAGrknenoArqyDo2mOMZuOUJGycS9EcEV9RYSfvbZYwmHKFUVXudqYT', '2019-02-21 01:00:37', '2019-02-21 01:00:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_actions`
--
ALTER TABLE `incident_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_approvals`
--
ALTER TABLE `incident_approvals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_actions`
--
ALTER TABLE `request_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_action_notes`
--
ALTER TABLE `request_action_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_approvals`
--
ALTER TABLE `request_approvals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_attachments`
--
ALTER TABLE `request_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_reasonfiles`
--
ALTER TABLE `request_reasonfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_sos`
--
ALTER TABLE `request_sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_so_bps`
--
ALTER TABLE `request_so_bps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`,`role_id`);

--
-- Indexes for table `service_coordinators`
--
ALTER TABLE `service_coordinators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incident_actions`
--
ALTER TABLE `incident_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incident_approvals`
--
ALTER TABLE `incident_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `request_actions`
--
ALTER TABLE `request_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `request_action_notes`
--
ALTER TABLE `request_action_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `request_approvals`
--
ALTER TABLE `request_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `request_attachments`
--
ALTER TABLE `request_attachments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `request_reasonfiles`
--
ALTER TABLE `request_reasonfiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_sos`
--
ALTER TABLE `request_sos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `request_so_bps`
--
ALTER TABLE `request_so_bps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `service_coordinators`
--
ALTER TABLE `service_coordinators`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11421;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
