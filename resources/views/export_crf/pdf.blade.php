<!DOCTYPE html>
<html>
<head>
	<title>Membuat Laporan PDF Dengan DOMPDF Laravel</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
	 @page  {
        margin: 0.1cm;
    }

		table tr td,
		table tr th{
			font-size: 8pt;
			overflow-wrap: break-word;
		}
	</style>
	<table class='table table-bordered'>
		<thead style="text-align:justify">
			<tr>
				
				<th>Tiket</th>
				<th>Layanan</th>
				<th>Judul</th>
				<th>Kebutuhan Bisnis</th>
				<th>Manfaat</th>
				<th>Peminta</th>
				<th>Telp & Email</th>
				<th>Tahap</th>
				<th>Priorty</th>
				<th>Difficulty</th>
				<th>Prepared</th>
				<th>Module</th>
				<th>Start Plan Date</th>
				<th>Finish Plan Date</th>
				<th>Stage BPS</th>
				<th>Created At</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $el)
			<tr>
				
				<td>{{$el->ticket}}</td>
				<td>{{$el->service->name}}</td>
				<td>{{$el->title}}</td>
				<td>{{wordwrap($el->business_need,25,"\n",TRUE)}}</td>
				<td>{{$el->business_benefit}}</td>
				<td>{{ucwords(strtolower($el->user->name))}}</td>
				<td>{{$el->telp}} <br> {{str_replace("@"," @",$el->user->email)}}</td>
				<td>{{$el->stage->name}}</td>
				<td>{{$el->priority->name}}</td>
				<td>{{$el->difficulty->name}}</td>
				<td>{{$el->preparedby}}</td>
				<td>{{$el->module}}</td>
				<td>{{$el->start_plan_date}}</td>
				<td>{{$el->finish_plan_date}}</td>
				<td>{{$el->action->name}}</td>
				<td>{{$el->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>