<table id="example" class="table table-bordered">
    <thead>
        <tr>
            <th>ID</th>
            <th>Tiket</th>
            <th>Layanan</th>
            <th>Judul</th>
            <th>Kebutuhan Bisnis</th>
            <th>Manfaat</th>
            <th>Peminta</th>
            <th>Telp - Email</th>
            <th>Tahap</th>
            <th>Priorty</th>
            <th>Difficulty</th>
            <th>Prepared By</th>
            <th>Module</th>
            <th>Start Plan Date</th>
            <th>Finish Plan Date</th>
            <th>Stage BPS</th>
            <th>Created At</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($data as $el)
        <tr>
            <td>{{$el->id}}</td>
            <td>{{$el->ticket}}</td>
            <td>{{$el->service->name}}</td>
            <td>{{$el->title}}</td>
            <td>{{$el->business_need}}</td>
            <td>{{$el->business_benefit}}</td>
            <td>{{$el->user->name}}</td>
            <td>{{$el->telp}} / {{$el->user->email}}</td>
            <td>{{$el->stage->name}}</td>
            <td>{{$el->priority->name}}</td>
            <td>{{$el->difficulty->name}}</td>
            <td>{{$el->preparedby}}</td>
            <td>{{$el->module}}</td>
            <td>{{$el->start_plan_date}}</td>
            <td>{{$el->finish_plan_date}}</td>
            <td>{{$el->action->name}}</td>
            <td>{{$el->created_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>