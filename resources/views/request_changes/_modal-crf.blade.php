<div class="modal fade" id="myModalCrf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="myModalLabel">Data BPS</h3>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped">
                                <tr>
                                    <th>Request Bps Id</th>
                                    <th>Modul aplikasi</th>
                                    <th>Tiket Kaseya</th>
                                    <th>Tahapan</th>
                                    <th> ... </th>
                                </tr>
                                @foreach ($requestchange as $item)
                                    <tr>
                                        <td>{{ isset($item->id) ? $item->id : '' }}</td>
                                        <td>{{ isset($item->service->name) ? $item->service->name : '' }}</td>
                                        <td>{{ isset($item->ticket_no) ? $item->ticket_no : '' }}</td>
                                        <td>{{ isset($item->stage->name) ? $item->stage->name : '' }}</td>
                                        <td><a target="_blank" href="{{ route('crf.view.bps', $item->id) }}" class="btn btn-success btn-sm id-modal"><span class="glyphicon glyphicon-eye-open"></span></a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
    
            </div>
        </div>
    </div>