<html>
    <head>
        <style>
            @page {
                margin-top: 30px;
                margin-left: 100px;
                margin-right: 100px;
            }

            .page-break {
                page-break-after: always;
            }
            /* 
            header {
                position: fixed;
                top: -60px;
                left: 0px;
                right: 0px;
                height: 70px;
                border-bottom: 1px solid black;
                text-align: center;
            } */

            header {
                left: 0px;
                right: 0px;
                border-bottom: 1px solid black;
                text-align: center;
            }

            .row {
                margin-top: 10px;
                margin-bottom: 10px;
            }

            table, tr, td {
                border:1px solid black;
            }

            td {
                padding: 5px;
            }
            
            img {
                margin: 0px;
            }

            br {
                margin: 0;
                }

            .title {
                font-size: 9px;
            }

            .header-crf {
                margin-top: 5px;
                text-align: center;
                
            }

            .title-crf {
                font-size: 24px;
            }
        </style>
    </head>
    <body>
        <header>
            <img src="{{public_path('images/logo_new.png')}}" alt="Logo" height=66px">
            <br>
            <label class="title" style="padding-top:0px">DIVISI BUSINESS ENABLER & ICT</label>
        </header>

        <div class="header-crf">
            <label class="title-crf">SERVICE REQUEST FORM (SRF)</label>
        </div>
                   
        <div class="col-md-12">
            <div class="row" style="margin-top: 20px">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Judul:</label>
                        <br/>
                        {{ isset($requests->title) ? $requests->title : '' }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="business_need">Kebutuhan Bisnis:</label>
                        <br/>
                        {{ isset($requests->business_need) ? $requests->business_need : ''}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="business_benefit">Manfaat Bisnis:</label>
                        <br/>
                        {{ isset($requests->business_benefit) ? $requests->business_benefit : '' }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="business_need">Peminta:</label>
                        <br/>
                        {{ isset($requests->user->name) ? $requests->user->name : ''}} ( {{ isset($requests->user->id) ? $requests->user->id :'' }} )
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="business_benefit">Contact</label>
                        <br/>
                        Email: {{ isset($requests->user->email) ? $requests->user->email : '' }}, Telp: {{ isset($requests->telp) ? $requests->telp: '' }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="reason">Tahapan: </label>
                        <br/>
                        {{ isset($requests->stage->name) ? $requests->stage->name : '' }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="reason">Nomor Tiket: </label>
                        <br/>
                        {{ isset($requests->ticket) ? $requests->ticket:''}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">Lampiran User</div>
                            <div class="panel-body">
                                @foreach($requests->requestAttachments as $item)
                                    <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                    <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
        </div>
        <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">RIWAYAT SRF</div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                    <thead class="table-success">
                                        <tr>
                                            <td>Tanggal</td>
                                            <td>Pengguna</td>
                                            <td>Aksi</td>
                                        </tr>
                                    </thead>
                                        <tbody class="table-success">
                                            
                                                @foreach ($requests->requestActions as $item)
                                                    <tr>
                                                        <td>{{$item->created_at}}</td>
                                                        <td>
                                                            {{$item->users->name}}
                                                            <br/>
                                                            Note:  {{ $item->requestActionNotes == "" ? '' : $item->requestActionNotes->note }}
                                                        </td>
                                                        <td>{{$item->action->name}}</td>
                                                    </tr>
                                                @endforeach
                                            
                                        </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
    </body>
</html>