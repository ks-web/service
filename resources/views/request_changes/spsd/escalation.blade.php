@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan: Eskalasi ke Modul Aplikasi</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    {{-- start form --}}
                    <form method="POST" action="{{ route('crf.spsd.escalation', $requestchange->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        {{-- title --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$requestchange->title}}
                                </div>
                            </div>
                        </div>

                        {{-- business need and business benefit --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_need }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_benefit }}
                                </div>
                            </div>
                        </div>

                        {{-- informasi user: usern identity, email, telp and contact --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $requestchange->user->name }} ( {{ $requestchange->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $requestchange->user->email }}, Tepl: {{ $requestchange->telp }}
                                </div>
                            </div>
                        </div>

                        {{-- this is atachment from user --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        {{-- checkbox choice modul aplication to escalation --}}
                        {{-- modul application  ==  service owner application --}}
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Pilih Modul Aplikasi</div>
                                <div class="panel-body">
                                    @foreach ($services as $item)
                                        <div class="form-check">
                                            <input name="so[]" 
                                            class="form-check-input" type="checkbox" value="{{$item->id}}">
                                            <label class="form-check-label" for="invalidCheck2">
                                                {{$item->name}}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        {{-- submit button --}}
                        <div class="form-group">
                                <br/>
                                <div class="btn-group mb-3" role="group">
                                    <button type="submit" class="btn btn-primary">
                                        Pilih Modul Aplikasi
                                    </button>
                                </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection