{{-- history --}}
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">RIWAYAT CRF</div>
            <div class="panel-body">
                <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <td>Tanggal</td>
                                <td>Pengguna</td>
                                <td>Aksi</td>
                            </tr>
                        </thead>
                        <tbody class="table-success">	
                            @foreach ($requestchange->requestChangeActions as $item)
                                <tr>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        {{$item->user->name}}
                                        <br/>
                                        <br/>
                                        {{ $item->requestChangeActionNotes == "" ? '' :'Note: '. $item->requestChangeActionNotes->note }}
                                    </td>
                                    <td>{{$item->action->name}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                        {{-- <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                            <tr>
                                <td colspan="6"><b>PENYELESAIAN</b></td>
                            </tr>
                            <tr>
                                <td colspan="6">{{$request->detail}}</td>
                            </tr>
                        </tbody>
                    </table> --}}
                </div>
            </div>
        </div>
    </div>
</div>