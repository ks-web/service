@extends('layouts.app')
@section('content')
<script>
    function myFunction() {
        var copyText = document.getElementById("bussiness_note");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function copyTitle() {
        var copyText = document.getElementById("title");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyBusinessBenefit() {
        var copyText = document.getElementById("business_benefit");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyKeterangan() {
        var copyText = document.getElementById("keterangan");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    $('#demodate').datetimepicker({
        inline:true,
    });

    function ceking(isi){
        $('#aksi').val(isi);
        return true;
    }
</script>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form permintaan / perubahan layanan (CRF)</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('crf.so.approval', $requestchange->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$requestchange->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_need }}
                                </div>
                            </div>
                        </div>
                        <div class='row'>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $requestchange->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $requestchange->user->name }} ( {{ $requestchange->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $requestchange->user->email }}, Tepl: {{ $requestchange->telp }}
                                </div>
                            </div>
                        </div>

                        {{--stage / tahap  --}}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ $requestchange->stage->name }}
                                </div>
                            </div>
                        </div>
                        
                        {{-- lampiran --}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($requestchange->requestChangeAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        {{-- note --}}
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea style="color:black;" id=note name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $requestchange->note }}</textarea>
                            <span class="invalid-feedback validasinote" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>

                        {{-- menambahkan lampiran --}}
                        <div class="form-group">
                            <label for="reason">Lampiran: Format file (image/pdf )</label>
                            <div class="input-group control-group increment">
                                <div class="custom-file">
                                    <input style="color:black;" class="custom-file-input" id="attachment" type="file" name="attachment[]" class="form-control">
                                </div>
                                <div class="input-group-btn"> 
                                    <button style="color:black;" class="btn btn-success tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>
                            </div>
                            <div>
                                <span class="invalid-feedback validasiattachment" role="alert">
                                    <strong>{{ $errors->first('attachment') }}</strong>
                                </span>
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <div class="custom-file">
                                        <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                    </div>
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- history --}}
                        @include('request_changes._riwayat')

                        {{-- jenis aksi 1 stuju 2 tolak --}}
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <input type="hidden" name="aksi" value="" id="aksi" />
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('aksi') }}</strong>
                                </span>
                            </div>
                        </div>

                        {{-- tombo tolak --}}
                        <div class="form-group">
                            <div class="btn-group btn-group-lg" role="group">
                                <button type="submit" class="btn btn-danger" id="idtolak" onclick="return ceking(2)">
                                    Tolak
                                </button>
                                <button type="submit" class="btn btn-primary" id="idsetuju" onclick="return ceking(1)">
                                    Setuju
                                </button>
                            </div>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection