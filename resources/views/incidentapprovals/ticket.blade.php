@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form Input Tiket Kaseya Pelayanan Incident</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('incidents.ticketcreated', $incident->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="description">Deskripsi:</label>
                                    <br/>
                                    {{ $incident->description }}
                                    
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="impact">Dampak:</label>
                                    <br/>
                                    {{ $incident->impact }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="priority">Prioritas:</label>
                                    <br/>
                                    {{$incident->priority->name}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="severity">Dampak Cakupan:</label>
                                    <br/>
                                    {{$incident->severity->name}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telp">Peminta:</label>
                                    <br/>
                                    {{ $incident->user->name }} ({{ $incident->user->id }})
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telp">No Telp:</label>
                                    <br/>
                                    {{ $incident->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telp">Lokasi:</label>
                                    <br/>
                                    {{ $incident->location }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="telp">Stage:</label>
                                    <br/>
                                    {{ $incident->stage->name }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran:</div>
                                        <div class="panel-body">
                                            @foreach($incident->incidentAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> {{$item->alias}}<br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>RIWAYAT</b></div>
                            <div class="panel-body">
                                <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%" >
                                        <tbody class="table-success">
                                            <tr>
                                                <td><b>Tanggal</b></td>
                                                <td><b>Pengguna</b></td>
                                                <td><b>Aksi</b></td>
                                            </tr>
                                        </tbody>
                                        <tbody class="table-success">	
                                            @foreach ($incident->incidentActions as $item)
                                                <tr>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>
                                                        {{$item->users->name}}
                                                    </td>
                                                    <td>
                                                        {{$item->action->name}}
                                                        <br/>
                                                        <br/>
                                                        Note: {{ $item->incidentActionNotes == "" ? '' : $item->incidentActionNotes->note }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ticket">Ticket</label>
                            <input type="text" name="ticket" class="form-control {{ $errors->has('ticket') ? ' is-invalid' : '' }}" autofocus>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ticket') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection