@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Input Detail Pelayanan Incident</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('incidents.detailsave', $incident->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="impact">Dampak</label>
                            <textarea readonly  name="impact" rows="8" class="form-control {{ $errors->has('impact') ? ' is-invalid' : '' }}" autofocus>{{ $incident->impact }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('impact') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea readonly  name="description" rows="8" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" autofocus>{{ $incident->description }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="priority">Prioritas</label>
                            <input readonly type="text" name="priority" class="form-control {{ $errors->has('priority') ? ' is-invalid' : '' }}" value="{{$priority}}" autofocus />
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('priority') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="severity">Dampak Cakupan</label>
                            <input readonly type="text" name="severity" class="form-control {{ $errors->has('severity') ? ' is-invalid' : '' }}" value="{{$incident->severity->name}}" autofocus />
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('severity') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran </div>
                                <div class="panel-body">
                                    @foreach($incident->incidentAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> {{$item->alias}}<br/><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="telp">No Telp</label>
                            <input readonly value="{{ $incident->telp }}" name="telp" type="text" class="form-control {{ $errors->has('telp') ? ' is-invalid' : '' }}" autofocus />
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="detail">Detail Layanan Incident</label>
                            <textarea name="detail" rows="8" class="form-control {{ $errors->has('detail') ? ' is-invalid' : '' }}" autofocus></textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('detail') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection