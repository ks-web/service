@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">Form Input Detail Pelayanan Incident</div>
            <div class="panel-body">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <form method="POST" action="{{route('incidents.rejectshow', $incident->id)}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="description">Deskripsi:</label>
                                <br/>
                                {{ $incident->description }}
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="impact">Dampak:</label>
                                <br/>
                                {{ $incident->impact }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="priority">Prioritas:</label>
                                <br/>
                                {{$incident->priority->name}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="severity">Dampak Cakupan:</label>
                                <br/>
                                {{$incident->severity->name}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="telp">No Telp:</label>
                                <br/>
                                {{ $incident->telp }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="ticket">Ticket Kaseya</label>
                                <br/>
                                {{ $incident->ticket }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="detail">Detail Layanan Incident:</label>
                                <br/>
                                {{ $incident->detail }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="detail">Target Selesai:</label>
                                <br/>
                                {{ $incident->duedate }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Lampiran:</div>
                                    <div class="panel-body">
                                        @foreach($incident->incidentAttachments as $item)
                                            <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> {{$item->alias}}<br/><br/>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <textarea name="note" rows="8" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ $incident->note }}</textarea>
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('note') }}</strong>
                        </span>
                    </div>
                    <div class="form-group">
                        <label for="reason">Upload Lampiran</label>
                        <div class="input-group control-group increment">
                            <div class="custom-file">
                                <input style="color:black;" class="custom-file-input" type="file" name="attachment[]" class="form-control">
                            </div>
                            <div class="input-group-btn"> 
                                <button style="color:black;" class="btn btn-success tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('attachment') }}</strong>
                            </span>
                        </div>
                        <div class="clone hide">
                            <div class="control-group input-group" style="margin-top:10px">
                                <div class="custom-file">
                                    <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                </div>
                                <div class="input-group-btn"> 
                                    <button class="btn btn-danger kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection