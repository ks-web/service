<table id="example" class="table table-bordered">
    <thead>
        <tr>
            <th>Tiket</th>
            <th>Judul</th>
            <th>Layanan</th>
            <th>Peminta</th>
            <th>Kebutuhan Bisnis</th>
            <th>Manfaat Bisnis</th>
            <th>Tanggal</th>
            <th>Tanggal Selesai</th>
            <th>Telp - Email</th>
            <th>Link Lampiran</th>
            <th>Stage Terakhir</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($data as $el)
        <tr>
            <td>{{$el->ticket}}</td>
            <td>{{$el->title}}</td>
            <td>{{$el->service->name}}</td>
            <td >{{$el->user->name}}</td>
            <td>{{$el->business_need}}</td>
            <td>{{$el->business_benefit}}</td>
            <td>{{$el->created_at}}</td>
            <td>{{$el->plan_date}}</td>
            <td>{{$el->telp}} / {{$el->user->email}}</td>
            <td>{{$el->attachment}}</td>
            <td>{{$el->stage->name}}</td>
        </tr>
        @endforeach
    </tbody>
</table>