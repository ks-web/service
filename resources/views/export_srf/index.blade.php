@extends('layouts.app')
@section('content')
{{-- notifikasi form validasi --}}
@if ($errors->has('file'))
<span class="invalid-feedback" role="alert">
    <strong>{{ $errors->first('file') }}</strong>
</span>
@endif

{{-- notifikasi sukses --}}
@if ($sukses = Session::get('sukses'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $sukses }}</strong>
</div>
@endif

<input type="date" id="mulai" value="{{$mulai}}">
<input type="date" id="sampai" value="{{$sampai}}"   style="margin-right: 10px">
<button type="submit" onclick="filter()" class="btn btn-sm btn-info">Search</button>
<a href="{{url('export-srf')}}" class="btn btn-sm btn-warning">Refresh</a>
<button type="submit" onclick="cari()" class="btn btn-sm btn-success">Export Excel</button>
<button type="submit" onclick="pdf()" class="btn btn-sm btn-danger">Cetak PDF</button>


<br>

<div class="panel panel-default">
        <div class="panel-body">
        <div class="table-responsive">
            @if ($data != null)
            <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Tiket</th>
                        <th>Judul</th>
                        <th>Layanan</th>
                        <th>Peminta</th>
                        <th>Kebutuhan Bisnis</th>
                        <th>Manfaat Bisnis</th>
                        <th>Tanggal</th>
                        <th>Tanggal Selesai</th>
                        <th>Telp</th>
                        <th>Lampiran</th>
                        <th>Stage Terakhir</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($data as $el)
                    <tr>
                        <td>{{$el->ticket}}</td>
                        <td>{{$el->title}}</td>
                        <td>{{$el->service->name}}</td>
                        <td >{{$el->user->name}}</td>
                        <td>{{$el->business_need}}</td>
                        <td>{{$el->business_benefit}}</td>
                        <td>{{$el->created_at}}</td>
                        <td>{{$el->plan_date}}</td>
                        <td>{{$el->telp}} </td>
						
						@if($el->attachment!='')
                        <td><a href="{{url('')}}/{{$el->attachment}}" target="_blank"><i class='fa fa-image'></i></a></td>
						
						@else
						
							<td></td>
					
					@endif
					
                        <td>{{$el->stage->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p>Loading...</p>
            @endif
         </div>
    </div>
</div>

    
@endsection

@push('js')

<script>
  var table = $('#example').DataTable();

  function cari(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

    location.assign("{{url('export-srf/export')}}?mulai="+mulai+"&sampai="+sampai);
  }

  function filter(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

    location.assign("{{url('export-srf')}}?mulai="+mulai+"&sampai="+sampai);
  }

  function pdf(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

   window.open("{{url('cetak-pdf-srf')}}?mulai="+mulai+"&sampai="+sampai,'_blank');
  }
</script>
    
@endpush


