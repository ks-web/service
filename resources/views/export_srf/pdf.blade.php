<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		@page  {
        margin: 0.2cm;
    	}

		table tr td,
		table tr th{
			font-size: 7pt;
			margin: 0px;
		}
	</style>
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th>Tiket</th>
				<th>Judul</th>
				<th>Layanan</th>
				<th>Peminta</th>
				<th>Kebutuhan Bisnis</th>
				<th>Manfaat Bisnis</th>
				<th>Tanggal</th>
				<th>Tanggal Selesai</th>
				<th>Telp</th>
				<th>Link Lampiran</th>
				<th>Stage Terakhir</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $el)
			<tr>
				<td>{{$el->ticket}}</td>
				<td>{{$el->title}}</td>
				<td>{{$el->service->name}}</td>
				<td>{{$el->user->name}}</td>
				<td>{{$el->business_need}}</td>
				<td>{{$el->business_benefit}}</td>
				<td>{{$el->created_at}}</td>
				<td>{{$el->plan_date}}</td>
				<td>{{$el->telp}} {{$el->user->email}}</td>
				<td>
					<a href="{{url('')}}/{{$el->attachment}}">File</a>
				</td>
				<td>{{$el->stage->name}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>