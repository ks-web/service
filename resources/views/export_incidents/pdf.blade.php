<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<table class='table table-bordered'>
		<thead>
			<tr>
                <th>Ticket</th>
                <th>Deskripsi</th>
                <th>Dampak</th>
                <th>Prioritas</th>
                <th>Detail Layanan</th>
                <th>User</th>
                <th>Tahap</th>
                <th>Created At</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $el)
			<tr>
                <td>{{$el->ticket}}</td>
                <td>{{$el->description}}</td>
                <td>{{$el->impact}}</td>
                <td>{{$el->priority->name}}</td>
                <td>{{$el->detail}}</td>
                <td>{{$el->user->name}}</td>
                <td>{{$el->stage->name}}</td>
                <td>{{$el->created_at}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
 
</body>
</html>