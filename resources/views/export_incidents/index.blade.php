@extends('layouts.app')
@section('content')
{{-- notifikasi form validasi --}}
@if ($errors->has('file'))
<span class="invalid-feedback" role="alert">
    <strong>{{ $errors->first('file') }}</strong>
</span>
@endif

{{-- notifikasi sukses --}}
@if ($sukses = Session::get('sukses'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong>{{ $sukses }}</strong>
</div>
@endif


    <input type="date" id="mulai" value="{{$mulai}}">
    <input type="date" id="sampai" value="{{$sampai}}"   style="margin-right: 10px">
    <button type="submit" onclick="filter()" class="btn btn-sm btn-info">Search</button>
    <a href="{{url('export-incidents')}}" class="btn btn-sm btn-warning">Refresh</a>
    <button type="submit" onclick="cari()" class="btn btn-sm btn-success">Export Excel</button>
    <button type="submit" onclick="pdf()" class="btn btn-sm btn-danger">Cetak PDF</button>

<br>

<div class="panel panel-default">
        <div class="panel-body">
        <div class="table-responsive">
            @if ($data != null)
                
            <table id="example" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Ticket</th>
                        <th>Deskripsi</th>
                        <th>Dampak</th>
                        <th>Prioritas</th>
                        <th>Detail Layanan</th>
                        <th>User</th>
                        <th>Tahap</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($data as $el)
                    <tr>
                        <td>{{$el->ticket}}</td>
                        <td>{{$el->description}}</td>
                        <td>{{$el->impact}}</td>
                        <td>{{$el->priority->name}}</td>
                        <td>{{$el->detail}}</td>
                        <td>{{$el->user->name}}</td>
                        <td>{{$el->stage->name}}</td>
                        <td>{{$el->created_at}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p>Loading...</p>
            @endif
         </div>
    </div>
</div>

    
@endsection

@push('js')

<script>
  var table = $('#example').DataTable();

  function cari(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

    location.assign("{{url('export-incidents/export')}}?mulai="+mulai+"&sampai="+sampai);
  }

  function filter(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

    location.assign("{{url('export-incidents')}}?mulai="+mulai+"&sampai="+sampai);
  }

  function pdf(){
    var mulai = $('#mulai').val();
    var sampai = $('#sampai').val();

    window.open("{{url('cetak-pdf-incidents')}}?mulai="+mulai+"&sampai="+sampai,'_blank');
  }
</script>
    
@endpush


