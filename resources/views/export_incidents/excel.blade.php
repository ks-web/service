<table id="example" class="table table-bordered">
    <thead>
        <tr>
            <th>Tickets</th>
            <th>Deskripsi</th>
            <th>Dampak</th>
            <th>Prioritas</th>
            <th>Detail Layanan</th>
            <th>User</th>
            <th>Tahap</th>
            <th>Created At</th>
        </tr>
    </thead>
    
    <tbody>
        @foreach ($data as $el)
        <tr>
            <td>{{$el->ticket}}</td>
            <td>{{$el->description}}</td>
            <td>{{$el->impact}}</td>
            <td>{{$el->priority->name}}</td>
            <td>{{$el->detail}}</td>
            <td>{{$el->user->name}}</td>
            <td>{{$el->stage->name}}</td>
            <td>{{$el->created_at}}</td>
        </tr>
        @endforeach
    </tbody>
</table>