@extends('layouts.app')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading"><b>Dashboard</b></div>
        <div class="panel-body">
            {{-- mulai tampilan data dasboard coordinator aplikasi --}}
            @role('coordinator aplikasi|coordinator infra')
                <div class="row">
                    <a href="{{ route('requests.index') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Menunggu Persetujuan</h3> 
                                    <h1 style="text-align:center">{{$waiting_co_approved_count}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('requests.viewperstage', 'all') }}">
                        <div class="col-sm-6 col-md-4 mx-auto">
                            <div class="thumbnail">
                                <div class="caption alert alert-success">
                                    <h3 style="text-align:center">Semua Permintaan</h3> 
                                    <h1 style="text-align:center">{{$all}}</h1>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endrole
            {{-- end tampilan data dasboard coordinator aplikasi --}}
        </div> 
</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}
</script>
@endsection