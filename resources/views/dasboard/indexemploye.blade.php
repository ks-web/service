@extends('layouts.app')
@section('content')
@role('employee')
<div class="panel panel-default">
    <div class="panel-heading"><b>Permintaan Layanan</b></div>
        <div class="panel-body">
        {{-- Tampilan dasboard untuk employee --}}
        <a href="{{ route('requests.index') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Dalam proses</h3> 
                        <h1 style="text-align:center">{{$jumlah_request_sedang_berjalan}}</h1>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('requests.viewperstage', '11') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Ditolak</h3> 
                        <h1 style="text-align:center">{{$jumlah_request_rejected}}</h1>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('requests.viewperstage', '9') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Selesai</h3> 
                        <h1 style="text-align:center">{{$jumlah_request_closed}}</h1>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading"><b>Laporan Gangguan</b></div>
        <div class="panel-body">
        <a href="{{ route('incidents.index') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Dalam proses</h3> 
                        <h1 style="text-align:center">{{$jumlah_sedang_berjalan}}</h1>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('incidents.showperstage', '5') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Menunggu Konfirmasi</h3> 
                        <h1 style="text-align:center">{{$jumlah_incident_waiting_user_conf}}</h1>
                    </div>
                </div>
            </div>
        </a>
        <a href="{{ route('incidents.showperstage', '9') }}">
            <div class="col-sm-6 col-md-4 mx-auto">
                <div class="thumbnail">
                    <div class="caption alert alert-success">
                        <h3 style="text-align:center">Selesai</h3> 
                        <h1 style="text-align:center">{{$jumlah_sudah_diclose}}</h1>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
@endrole
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}
</script>
@endsection