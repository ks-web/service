@extends('layouts.app')
@section('content')
<script>
$(document).ready(function(){
    $('#simpan').click(function() {
        $(this).attr('disabled','disabled');
        $(this).attr('value', 'Sedang mengirim...')
        $('#formField').submit();
    });
});
</script>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form id="formField" method="POST" action="{{ route('incidents.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="description">Deskripsi</label>
                            <textarea  name="description" rows="6" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" autofocus>{{ old('description') }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                        </div>
                        <div class="form-group">
                            <label for="impact">Dampak</label>
                            <textarea  name="impact" rows="6" class="form-control {{ $errors->has('impact') ? ' is-invalid' : '' }}" autofocus>{{ old('impact') }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('impact') }}</strong>
                                </span>
                        </div>
                        <div class="form-group">
                            <label for="priority_id">Prioritas</label>
                            <select name="priority_id" class="form-control {{ $errors->has('priority_id') ? ' is-invalid' : '' }}">
                                @foreach ($priorities as $priority)
                                <option selected value={{$priority->id}}>{{$priority->name}}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('priority_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="severity_id">Cakupan Dampak</label>
                            <select name="severity_id" class="form-control {{ $errors->has('severity_id') ? ' is-invalid' : '' }}">
                                @foreach ($severities as $severity)
                                <option selected value={{$severity->id}}>{{$severity->name}}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('severity_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="telp">No Telp</label>
                            <input name="telp" type="text" class="form-control {{ $errors->has('telp') ? ' is-invalid' : '' }}" autofocus />
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telp') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="location">Lokasi</label>
                            <input name="location" type="text" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" autofocus />
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('location') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="reason">Lampiran (Format file: image/pdf)</label>
                            <div class="input-group control-group increment">
                                <div class="custom-file">
                                    <input style="color:black;" class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                </div>
                                <div class="input-group-btn"> 
                                    <button style="color:black;" class="btn btn-success tambah1" type="button"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('attachment') }}</strong>
                                </span>
                            </div>
                            <div class="clone hide">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <div class="custom-file">
                                        <input class="custom-file-input" type="file" name="attachment[]" class="form-control">
                                    </div>
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger kurang1" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <input type="submit" class="btn btn-primary" id="simpan" value="Simpan" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection