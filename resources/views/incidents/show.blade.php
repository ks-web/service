@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">Detail Gangguan</div>
            <div class="panel-body">
                <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                    <tbody class="table-success">
                        <tr>
                            <td><b>ID</b></td><td>:</td><td>{{$incident->id}}</td>
                            <td><b>DAMPAK</b></td><td>:</td><td>{{$incident->impact}}</td>
                        </tr>
                        <tr>
                            <td><b>DESKRIPSI</b></td><td>:</td><td>{{$incident->description}}</td>
                            <td><b>PEMINTA</b></td><td>:</td><td>{{$incident->user->idWithName}}</td>
                        </tr>
                        <tr>
                            <td><b>PRIORITAS</b></td><td>:</td><td>{{$incident->priority->name}}</td>
                            <td><b>DAMPAK CAKUPAN</b></td><td>:</td><td>{{$incident->severity->name}}</td>
                        </tr>
                        <tr>
                            <td><b>TIKET KASEYA</b></td><td>:</td><td>{{$incident->ticket}}</td>
                            <td><b>TAHAP</b></td><td>:</td><td>{{$incident->stage->name}}</td>
                        </tr>
                        <tr>
                            <td><b>NOMOR TELP</b></td><td>:</td><td>{{$incident->telp}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><b>DATA LAMPIRAN</b></div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <tbody class="table-success">
                            <tr>
                                <td colspan="12">
                                    @foreach($incident->incidentAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                        <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}} </a>
                                        <br/>
                                        <br/>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><b>USER DENIED LAMPIRAN</b></div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <tbody class="table-success">
                            <tr>
                                <td colspan="12">
                                    @foreach($incident->incidentRejectattachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                        <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}} </a>
                                        <br/>
                                        <br/>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><b>RIWAYAT</b></div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive" cellspacing="0" width="100%" style="overflow-y: scroll; height:300px;">
                        <tbody class="table-success">
                            <tr>
                                <td><b>Tanggal</b></td>
                                <td><b>Pengguna</b></td>
                                <td><b>Aksi</b></td>
                            </tr>
                        </tbody>
                        <tbody class="table-success">	
                            @foreach ($incident->incidentActions as $item)
                                <tr>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        {{$item->users->name}}
                                    </td>
                                    <td>
                                        {{$item->action->name}}
                                        <br/>
                                        <br/>
                                            Note: {{ $item->incidentActionNotes == "" ? '' : $item->incidentActionNotes->note }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection