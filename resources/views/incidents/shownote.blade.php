@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading"><b>RIWAYAT</b></div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                        <tbody class="table-success">
                            <tr>
                                <td><b>Tanggal</b></td>
                                <td><b>Pengguna</b></td>
                                <td><b>Aksi</b></td>
                            </tr>
                        </tbody>
                        <tbody class="table-success">	
                            @foreach ($incident->incidentActions as $item)
                                <tr>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        {{$item->users->name}}
                                    </td>
                                    <td>
                                        {{$item->action->name}}
                                        <br/>
                                        <br/>
                                        @foreach ($item->incidentActionNotes as $note)
                                            Note:
                                            <br/>
                                            {{$note->note}}
                                        @endforeach
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection