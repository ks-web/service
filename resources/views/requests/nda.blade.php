<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Kebijakan dan Aturan Penggunaan Layanan</h4>
            </div>
            <div class="modal-body">
                Kebijakan dan Aturan Pengguna layanan harus:
                <br/>
                <br/>
                <ol>
                    <li>Mentaati dan mematuhi seluruh peraturan, regulasi dan perundang-undangan yang mengatur tentang data, informasi dan perangkat elektronik yang berlaku di Negara Kesatuan Republik Indonesia.</li>
                    <li>Mentaati kebijakan penggunaan layanan jaringan intranet PT Krakatau Steel (Persero) Tbk.</li>
                    <li>Mentaati kebijakan penggunaan userid dan password di PT Krakatau Steel (Persero) Tbk.</li>
                    <li>Menjaga rahasia PT Krakatau Steel (Persero) Tbk.terkait data dan informasi yang didapat dengan adanya penggunaan jaringan intranet PT Krakatau Steel (Persero) Tbk. ini.</li>
                    <li>Menjaga keamanan data dan informasi yang saya dapatkan dengan adanya penggunaan perangkat pribadi di jaringan PT Krakatau Steel (Persero) Tbk.</li>
                    <li>Tidak melakukan instalasi perangkat lunak hasil bajakan atau perangkat lunak hasil cracking pada perangkat pribadi tersebut (baik saya lakukan sendiri atau dengan dengan bantuan pihak lain).</li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>