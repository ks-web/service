@extends('layouts.app')
@section('content')
<p>&nbsp;</p>
<div class="panel panel-default">
	<div class="panel-heading"><b>RIWAYAT</b></div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-responsive" cellspacing="0" width="100%">
					<tbody class="table-success">
						<tr>
							<td><b>Tanggal</b></td>
							<td><b>Pengguna</b></td>
							<td><b>Aksi</b></td>
						</tr>
						<tbody class="table-success">	
							@foreach ($request->requestActions as $item)
								<tr>
									<td>{{$item->created_at}}</td>
									<td>
										{{$item->users->name}}
										<br/>
										<br/>
										@foreach($item->requestActionNotes as $tes)
											Note: {{$tes->note}}
										@endforeach
									</td>
									<td>{{$item->action->name}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<table class="table table-striped table-responsive" cellspacing="0" width="100%">
						<tr>
							<td colspan="6"><b>DETAIL LAYANAN</b></td>
						</tr>
						<tr>
							<td colspan="6">{{$request->detail}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}
</script>
@endsection
