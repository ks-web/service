<div class="modal fade" id="modalBro" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body" id="test">
				<div class="row">
                    <div class="col-lg-6">
                            <div class="form-group">
                                <label>Judul:</label>
                                <br/>result.request.title</div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="service_id">Layanan:</label>
                            <br/>result.request.service.name</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group" >
                            <label for="categories">Kategori Layanan:</label>
                            <br/>result.request.category.name</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_need">Kebutuhan Bisnis:</label>
                            <br/>result.request.business_need</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Bisnis:</label>
                            <br/>result.request.business_benefit</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_need">Nomor Tiket Kaseya:</label>
                            <br/>result.request.ticket</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_benefit">Detail Pelayanan:</label>
                            <br/>result.request.detail</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_need">Target Selesai:</label>
                            <br/>result.request.end_date</div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="business_need">Stage:</label>
                            <br/>result.request.stage.name</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>LAMPIRAN BPS</b></div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                        <tbody class="table-success">
                                            <tr>
                                                <td colspan="12">
                                                            
                                                        
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>LAMPIRAN USER</b></div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                        <tbody class="table-success">
                                            <tr>
                                                <td colspan="12">;
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading"><b>RIWAYAT</b></div>
                            <div class="panel-body">
                                <div class="table-responsive" style="overflow-y: scroll; height:250px;">
                                    <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                        <tbody class="table-success">
                                            <tr>
                                                <td><b>Tanggal</b></td>
                                                <td><b>Pengguna</b></td>
                                                <td><b>Aksi</b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
