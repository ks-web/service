@extends('layouts.app')
@section('content')
<script>
    function myFunction() {
        var copyText = document.getElementById("bussiness_note");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }
    
    function copyTitle() {
        var copyText = document.getElementById("title");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyBusinessBenefit() {
        var copyText = document.getElementById("business_benefit");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    function copyKeterangan() {
        var copyText = document.getElementById("keterangan");
        copyText.select();
        document.execCommand("copy");
        //alert("Copied the text: " + copyText.value);
    }

    $('#demodate').datetimepicker({
        inline:true,
    });
</script>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Form Permintaan Layanan: Input Nomor Tiket Kaseya</b></div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('requests.updateticket', $request->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$request->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="service_id">Layanan:</label>
                                    <br/>
                                    {{$request->service->name}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group" >
                                    <label for="categories">Kategori Layanan:</label>
                                    <br/>
                                    {{$request->category->name}}     
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $request->business_need }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $request->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Keterangan tambahan ( jenis aplikasi, level akses, dll): </label>
                                    <br/>
                                    {{ $request->keterangan }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Kebijakan dan Aturan:</label><br/>
                                    <label class="custom-control-label" for="customCheck1">Sudah menyetujui Kebijakan dan Aturan Penggunaan Layanan <span type="button" class="badge badge-danger" data-toggle="modal" data-target="#myModal"> View </span></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $request->user->name }} ( {{ $request->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $request->user->email }}, Tepl: {{ $request->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan: </label>
                                    <br/>
                                    {{ $request->stage->name }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($request->requestAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @role('operation ict')
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Alasan:</label>
                                    <br/>
                                    {{ $request->reason }}
                                </div>
                            </div>
                        </div>
                        @endrole
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>RIWAYAT</b></div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <tbody class="table-success">
                                                    <tr>
                                                        <td><b>Tanggal</b></td>
                                                        <td><b>Pengguna</b></td>
                                                        <td><b>Aksi</b></td>
                                                    </tr>
                                                    <tbody class="table-success">	
                                                        @foreach ($request->requestActions as $item)
                                                            <tr>
                                                                <td>{{$item->created_at}}</td>
                                                                <td>
                                                                    {{$item->users->name}}
                                                                    <br/>
                                                                    <br/>
                                                                    Note:  {{ $item->requestActionNotes == "" ? '' : $item->requestActionNotes->note }}
                                                                </td>
                                                                <td>{{$item->action->name}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="6"><b>PENYELESAIAN</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">{{$request->detail}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label for="ticket">Date</label>
                            <input id="demodate" type="text" name="demodate" values="{{ $request->demodate }}" class="form-control {{ $errors->has('demodate') ? ' is-invalid' : '' }}" autofocus>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('demodate') }}</strong>
                            </span>
                        </div> --}}
                        <div class="form-group">
                            <label for="ticket">Tiket Kaseya</label>
                            <input type="text" name="ticket" values="{{ $request->ticket }}" class="form-control {{ $errors->has('ticket') ? ' is-invalid' : '' }}" autofocus>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ticket') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <br/>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection