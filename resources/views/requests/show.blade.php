@extends('layouts.app')
@section('content')
<div class="row justify-content-center">
	<div class="col-md-10">
		<div class="panel panel-default">
			<div class="panel-heading"><b>DATA DETAIL</b></div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-responsive" cellspacing="0" width="100%">
						<tbody class="table-success">
							<tr>
								<td><b>ID</b></td><td>:</td><td>{{$request->id}}</td>
								<td><b>LAYANAN</b></td><td>:</td><td>{{$request->service->name}}</td>
							</tr>
							<tr>
								<td><b>KATEGORI</b></td><td>:</td><td>{{$request->category->name}}</td>
								<td><b>PEMINTA</b></td><td>:</td><td>{{$request->user->idWithName}}</td>
							</tr>
							<tr>
								<td><b>TANGGAL PERMINTAAN</b></td><td>:</td><td>{{$request->created_at}}</td>
								<td><b>TIKET</b></td><td>:</td><td>{{$request->ticket}}</td>
							</tr>
							<tr>
								<td><b>TAHAP SAAT INI</b></td><td>:</td><td>{{$request->stage->name}}</td>
							</tr>
							<tr>
								<td colspan="6"><b>ALASAN PERMINTAAN</b></td>
							</tr>
							<tr>
								<td colspan="6">{{$request->business_need}}</td>
							</tr>
							<tr>
								<td colspan="6"><b>MANFAAT TERHADAP BISNIS</b></td>
							</tr>
							<tr>
								<td colspan="6">{{$request->business_benefit}}</td>
							</tr>
							<tr>
								<td colspan="6"><b>KETERANGAN TAMBAHAN</b></td>
							</tr>
							<tr>
								<td colspan="6">{{$request->keterangan}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><b>DATA LAMPIRAN-LAMPIRAN</b></div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-responsive" cellspacing="0" width="100%">
						<tbody class="table-success">
							<tr>
								<td colspan="12"><b>LAMPIRAN</b></td>
							</tr>
							<tr>
								<td colspan="12">
									@foreach($request->requestAttachments as $item)
										<a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
										<a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}} </a>
										<br/>
										<br/>
									@endforeach
								</td>
							</tr>
							@if($request->category_id == 1)
							<tr>
								<td colspan="12"><b>LAMPIRAN BPS</b></td>
							</tr>
							@foreach($request->requestSos as $item)
							<tr>
								<td>
									{{$item->service->name}}
								</td>
							</tr>
								@foreach ($item->requestSoBps as $item)
								<tr>
									<td colspan="11">
										<a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}}</a>
										<br/>
									</td>
								</tr>
								@endforeach			
							@endforeach
							@endif
							<tr>
								<td colspan="12"><b>LAMPIRAN REKOMENDASI</b></td>
							</tr>
							<tr>
								<td colspan="12">
									@foreach($request->requestReasonfiles as $item)
										<a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
										<a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}}</a>
									@endforeach
								</td>
							</tr>
							<tr>
								<td colspan="12"><b>LAMPIRAN REJECT USER</b></td>
							</tr>
							<tr>
								<td colspan="12">
									@foreach($request->requestRejectattachments as $item)
										<a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
										<a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank"> {{$item->alias}}</a>
									@endforeach
								</td>
							</tr>
						</tbody>			
					</table>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading"><b>RIWAYAT</b></div>
			<div class="panel-body">
				<div class="table-responsive" style="overflow-y: scroll; height:300px;">
					<table class="table table-striped table-responsive" cellspacing="0" width="100%">
						<tbody class="table-success">
							<tr>
								<td><b>Tanggal</b></td>
								<td><b>Pengguna</b></td>
								<td><b>Aksi</b></td>
							</tr>
							<tbody class="table-success">	
								@foreach ($request->requestActions as $item)
									<tr>
										<td>{{$item->created_at}}</td>
										<td>
											{{$item->users->name}}
											<br/>
											<br/>
											Note:  {{ $item->requestActionNotes == "" ? '' : $item->requestActionNotes->note }}
										</td>
										<td>{{$item->action->name}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						<table class="table table-striped table-responsive" cellspacing="0" width="100%">
							<tr>
								<td colspan="6"><b>PENYELESAIAN</b></td>
							</tr>
							<tr>
								<td colspan="6">{{$request->detail}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
<script>
	function view(id){
		$.colorbox({
			iframe:true, 
			width:"80%", 
			height:"80%",
			transition:'none',
			title: "Preview Data"
			//href:"#"
		});
	}
</script>
@endsection
