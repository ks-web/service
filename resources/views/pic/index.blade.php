@extends('layouts.app')

@section('content')
<a href="{{route('pic.create')}}" class="control">
    <button class="btn btn-primary">Tambah Data</button>
</a>
<p>&nbsp;</p>
@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
<table id="dataMaster" class="display" cellspacing="0" width="100%">
    <thead class="table-success">
        <tr>
            <th class="center" width="5%"> ID </th>
            <th width="40%">NAMA</th>
            <th width="15%">CREATE AT</th>
            <th width="15%">UPDATE AT</th>
            <th width="15%">AKSI</th>
            <th width="10%">DELETE</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($paginated as $pic)
            <tr>
                <td>{{$pic->id}}</td>
                <td>{{$pic->name}}</td>
                <td>{{$pic->created_at}}</td>
                <td>{{$pic->updated_at}}</td>
                <td>
                    <a href="{{ route('pic.edit',$pic->id) }}" class="btn btn-primary"> Edit </a> 
                    <a href="#" onclick="view({{$pic->id}})" class="btn btn-primary"> View </a> 
                </td>
                <td>
                    <form method="POST" action="{{route('pic.destroy',$pic->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection