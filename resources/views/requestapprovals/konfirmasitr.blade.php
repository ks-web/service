@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('requests.trasportupdate', $request->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input readonly type="text" name="title" id="summernote" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{$request->title}}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('serviceid') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="service_id">Layanan</label>
                            <select readonly id="idservice" name="service_id" class="form-control {{ $errors->has('service_id') ? ' is-invalid' : '' }}">
                                    <option>Pilih Service</option>
                                @foreach ($services as $service)
                                    @if($service->id == $request->service_id)
                                        <option selected value={{$service->id}}>{{$service->name}}</option>
                                    @else
                                        <option value={{$service->id}}>{{$service->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('service_id') }}</strong>
                            </span>
                        </div>
                        <div class="form-group" >
                            <label for="categories">Kategori Layanan</label>
                            <select readonly id="idcategories" name="categories" class="form-control {{ $errors->has('categories') ? ' is-invalid' : '' }}">
                                <option>Pilih Kategori</option>
                                @foreach ($categories as $category)
                                @if($category->id == $request->category_id)
                                    <option selected value={{$category->id}}>{{$category->name}}</option>
                                @else
                                    <option value={{$category->id}}>{{$category->name}}</option>
                                @endif
                            @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('categories') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_need">Kebutuhan Bisnis</label>
                            <textarea readonly name="business_need" id="summernote" rows="6" class="form-control {{ $errors->has('business_need') ? ' is-invalid' : '' }}" autofocus>{{ $request->business_need }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('business_need') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="business_benefit">Manfaat Bisnis</label>
                            <textarea readonly name="business_benefit" rows="6" class="form-control {{ $errors->has('business_benefit') ? ' is-invalid' : '' }}" autofocus>{{ $request->business_benefit }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('business_benefit') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="reason">Keterangan tambahan ( jenis aplikasi, level akses, dll)</label>
                            <textarea readonly name="keterangan" rows="6" class="form-control {{ $errors->has('keterangan') ? ' is-invalid' : '' }}" autofocus>{{ $request->keterangan }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('keterangan') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="title">Tiket</label>
                            <input readonly type="text" name="tiket" id="summernote" rows="2" class="form-control {{ $errors->has('tiket') ? ' is-invalid' : '' }}" autofocus value="{{$request->ticket}}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ticket') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran User</div>
                                <div class="panel-body">
                                    @foreach($request->requestAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> 
                                        <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank">{{$item->alias}}</a><br/><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran Bps</div>
                                <div class="panel-body">
                                    @foreach($request->requestSos as $item)
                                        <label>SO {{$item->service->name}}</label><br/>
                                        @foreach($item->requestSoBps as $item)
                                            <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a> 
                                            <a class="" href="{{asset('storage/' . $item->attachment) }}" target="_blank">{{$item->alias}}</a><br/><br/>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input readonly type="hidden" name="aksi" id="aksi" rows="2" class="form-control {{ $errors->has('aksi') ? ' is-invalid' : '' }}" autofocus value="{{$request->aksi}}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('aksi') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="note">Note</label>
                            <textarea name="note" rows="6" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus></textarea>
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('note') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <br/>
                            <div class="btn-group mb-3" role="group">
                                <button type="submit" id="idsetuju"  class="btn btn-primary">
                                    Konfirmasi
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection