@extends('layouts.app')
@section('content')
<script>
$(document).ready(function(){
    $('#idalasan').hide();
    $('#idrecomend').hide();
    $('#iddivlampiran').hide();
    $('#idnote').hide();
    
    $('#document_status').click(function(){
        var id = $("#document_status").val();
        if(id == "l1")
        {
            $('#idalasan').show();
            $('#idrecomend').show();
            $('#iddivlampiran').show();
            $('#idnote').hide();
        }
        else if(id == "l2")
        {
            $('#idalasan').hide();
            $('#idrecomend').hide();
            $('#iddivlampiran').hide();
            $('#idnote').show();
        }
        else
        {
            $('#idnote').hide();
            $('#idalasan').hide();
            $('#idrecomend').hide();
            $('#iddivlampiran').hide();
        }
    });

});
</script>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('requests.updaterecomendation', $request->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Judul:</label>
                                    <br/>
                                    {{$request->title}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="service_id">Layanan:</label>
                                    <br/>
                                    {{$request->service->name}}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group" >
                                    <label for="categories">Kategori Layanan:</label>
                                    <br/>
                                    {{$request->category->name}}     
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Kebutuhan Bisnis:</label>
                                    <br/>
                                    {{ $request->business_need }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Manfaat Bisnis:</label>
                                    <br/>
                                    {{ $request->business_benefit }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Keterangan tambahan ( jenis aplikasi, level akses, dll): </label>
                                    <br/>
                                    {{ $request->keterangan }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Kebijakan dan Aturan:</label><br/>
                                    <label class="custom-control-label" for="customCheck1">Sudah menyetujui Kebijakan dan Aturan Penggunaan Layanan <span type="button" class="badge badge-danger" data-toggle="modal" data-target="#myModal"> View </span></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_need">Peminta:</label>
                                    <br/>
                                    {{ $request->user->name }} ( {{ $request->user->id }} )
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="business_benefit">Contact</label>
                                    <br/>
                                    Email: {{ $request->user->email }}, Tepl: {{ $request->telp }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Tahapan</label>
                                    <br/>
                                    {{ $request->stage->name }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Lampiran User</div>
                                        <div class="panel-body">
                                            @foreach($request->requestAttachments as $item)
                                                <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>
                                                <a href="{{asset('storage/' . $item->attachment) }}" target="_blank"> <span>{{$item->alias}}</span></a><br/><br/>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @role('operation ict')
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="reason">Alasan:</label>
                                    <br/>
                                    {{ $request->reason }}
                                </div>
                            </div>
                        </div>
                        @endrole
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b>RIWAYAT</b></div>
                                    <div class="panel-body">
                                        <div class="table-responsive" style="overflow-y: scroll; height:300px;">
                                            <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                <tbody class="table-success">
                                                    <tr>
                                                        <td><b>Tanggal</b></td>
                                                        <td><b>Pengguna</b></td>
                                                        <td><b>Aksi</b></td>
                                                    </tr>
                                                    <tbody class="table-success">	
                                                        @foreach ($request->requestActions as $item)
                                                            <tr>
                                                                <td>{{$item->created_at}}</td>
                                                                <td>
                                                                    {{$item->users->name}}
                                                                    <br/>
                                                                    <br/>
                                                                    Note: {{ $item->requestActionNotes == "" ? '' : $item->requestActionNotes->note }}
                                                                </td>
                                                                <td>{{$item->action->name}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                                <table class="table table-striped table-responsive" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="6"><b>PENYELESAIAN</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">{{$request->detail}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="document_status">Diteruskan Ke</label>
                            <select style="color:black;" require id="document_status" name="document_status" class="form-control {{ $errors->has('document_status') ? ' is-invalid' : '' }}">
                                <option value=""> -- Pilih -- </option>
                                <option value="l2" id="l2">Send to User </option>
                                <option value="l1" id="l1">Send to Super intedent BEICT </option>
                            </select>
                            <span class="invalid-feedback document_status" role="alert">
                                <strong>{{ $errors->first('document_status') }}</strong>
                            </span>
                        </div>
                        <div id="idnote" class="form-group">
                                <label for="note">Note</label>
                                <textarea  name="note" rows="4" class="form-control {{ $errors->has('note') ? ' is-invalid' : '' }}" autofocus>{{ old('note') }}</textarea>
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('note') }}</strong>
                                </span>
                            </div>
                        <div id="idalasan" class="form-group">
                            <label for="reason">Alasan</label>
                            <textarea  name="reason" rows="4" class="form-control {{ $errors->has('reason') ? ' is-invalid' : '' }}" autofocus>{{ old('reason') }}</textarea>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        </div>
                        <div class="form-group" id="idrecomend">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="recomendation_status" id="exampleRadios1" value="1">
                                <label class="form-check-label" for="exampleRadios1">
                                    Rekomendasi
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="recomendation_status" id="exampleRadios2" value="2">
                                <label class="form-check-label" for="exampleRadios2">
                                    Tidak Rekomendasi
                                </label>
                              </div>
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nda') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="btn-group mb-3" role="group">
                                <br/>
                                <button type="submit" class="btn btn-primary">
                                    Kirim
                                </button>
                            </div>
                        </div>
                        <!-- Modal -->
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection