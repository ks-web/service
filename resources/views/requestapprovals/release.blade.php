@extends('layouts.app')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Layanan</div>
                <div class="panel-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('requests.releasebps', $request->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <label for="title">Judul</label>
                            <input readonly type="text" name="title" id="summernote" rows="2" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" autofocus value="{{$request->title}}">
                            <input readonly type="hidden" name="serviceid" id="summernote" rows="2" class="form-control {{ $errors->has('serviceid') ? ' is-invalid' : '' }}" autofocus value="{{$requestSos->service_id}}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('serviceid') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="title">Tiket</label>
                            <input readonly type="text" name="tiket" id="summernote" rows="2" class="form-control {{ $errors->has('tiket') ? ' is-invalid' : '' }}" autofocus value="{{$request->ticket}}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ticket') }}</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran User</div>
                                <div class="panel-body">
                                    @foreach($request->requestAttachments as $item)
                                        <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>{{$item->alias}}<br/><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Lampiran Bps</div>
                                <div class="panel-body">
                                    @foreach($request->requestSos as $item)
                                        @foreach($item->requestSoBps as $item)
                                            <a class="btn btn-primary" href="{{asset('storage/' . $item->attachment) }}" target="_blank"><span class="glyphicon glyphicon-file"></span> File </a>{{$item->alias}}<br/><br/>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">Pilih SO</div>
                                <div class="panel-body">
                                    @foreach ($services as $item)
                                        @if($item->id == $request->service_id)
                                            <div class="form-check">
                                                <input name="so[]" checked class="form-check-input" type="checkbox" value="{{$item->id}}">
                                                <label class="form-check-label" for="invalidCheck2">
                                                    {{$item->name}}
                                                </label>
                                            </div>
                                        @else
                                            <div class="form-check">
                                                <input name="so[]" class="form-check-input" type="checkbox" value="{{$item->id}}">
                                                <label class="form-check-label" for="invalidCheck2">
                                                    {{$item->id}}
                                                    {{$item->name}}
                                                </label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                                <br/>
                                <div class="btn-group mb-3" role="group">
                                    <button type="submit" class="btn btn-primary">
                                        Rilis BPS
                                    </button>
                                </div>
                        </div>
                        @include('requests.nda')
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection