@extends('layouts.app')

@section('content')
<a href="{{route('level2.create')}}" class="control">
    <button class="btn btn-primary">Tambah Data</button>
</a>
<p>&nbsp;</p>
@if (session('success'))
    <div class="alert alert-success" role="alert">
        {{ session('success') }}
    </div>
@endif
<table id="dataMaster" class="display" cellspacing="0" width="100%">
    <thead class="table-success">
        <tr>
            <th class="center" width="5%"> ID </th>
            <th width="40%">NAMA</th>
            <th width="15%">Email</th>
            <th width="15%">Group Service</th>
            <th width="15%">AKSI</th>
            <th width="10%">DELETE</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($level2 as $services)
            <tr>
                <td>{{$services->nik}}</td>
                <td>{{$services->nama}}</td>
                <td>{{$services->email}}</td>
                <td>{{$services->groupserv}}</td>
                <td>
                    <a href="{{ route('level2.edit',$services->nik) }}" class="btn btn-primary"> Edit </a> 
                    <a href="#" onclick="view({{$services->nik}})" class="btn btn-primary"> View </a> 
                </td>
                <td>
                    <form method="POST" action="{{route('level2.destroy',$services->nik)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form> 
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection