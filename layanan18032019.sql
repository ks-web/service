-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2019 at 10:59 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `layanan`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'create', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(2, 'approve', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(3, 'reject', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(4, 'escalation', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(5, 'confirmation transport', '2019-02-17 09:39:43', '2019-02-17 09:39:43'),
(6, 'ticket input', '2019-02-17 09:41:56', '2019-02-17 09:41:56'),
(7, 'detail input', '2019-02-17 21:31:26', '2019-02-17 21:31:26'),
(8, 'nda aprove', '2019-02-18 07:44:45', '2019-02-18 07:44:45'),
(9, 'transport', '2019-02-18 15:30:47', '2019-02-18 15:30:47'),
(10, 'release', '2019-02-18 15:39:09', '2019-02-18 15:39:09'),
(11, 'upload bps', '2019-02-18 15:39:41', '2019-02-18 15:39:41'),
(12, 'recomendation', '2019-02-18 15:40:13', '2019-02-18 15:40:13'),
(13, 'not recomendation', '2019-02-18 15:45:50', '2019-02-18 15:45:50'),
(14, 'waiting document', '2019-02-18 15:48:20', '2019-02-18 15:48:20'),
(15, 'choose SO', '2019-02-18 15:49:10', '2019-02-18 15:49:10'),
(16, 'unrelease', '2019-03-12 17:00:00', '2019-03-12 17:00:00'),
(17, 'upload file', '2019-03-14 17:00:00', '2019-03-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'perubahan atau penambahan aplikasi', '2019-02-03 04:24:37', '2019-02-03 04:24:42'),
(2, 'permintaan akses/layanan', '2019-02-03 04:25:27', '2019-02-03 04:25:32');

-- --------------------------------------------------------

--
-- Table structure for table `incidents`
--

CREATE TABLE `incidents` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `impact` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `priority_id` int(10) UNSIGNED NOT NULL,
  `severity_id` int(10) UNSIGNED NOT NULL,
  `ticket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `incidents`
--

INSERT INTO `incidents` (`id`, `description`, `impact`, `user_id`, `stage_id`, `priority_id`, `severity_id`, `ticket`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'Tidak bisa login sso', 'Tidak bisa akses aplikasi eos dan resevarsi', 6833, 9, 2, 1, 'IN90001', 'Sudah di kerjakan sesuai permintaan, coba login kembali', '2019-03-18 01:57:20', '2019-03-18 02:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `incident_actions`
--

CREATE TABLE `incident_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `incident_id` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `action_type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `incident_actions`
--

INSERT INTO `incident_actions` (`id`, `incident_id`, `user`, `action_type`, `created_at`, `updated_at`) VALUES
(1, 1, 6833, 1, '2019-03-18 01:57:31', '2019-03-18 01:57:31'),
(2, 1, 3, 6, '2019-03-18 02:10:55', '2019-03-18 02:10:55'),
(3, 1, 3, 7, '2019-03-18 02:14:07', '2019-03-18 02:14:07'),
(4, 1, 6833, 2, '2019-03-18 02:14:25', '2019-03-18 02:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `incident_approvals`
--

CREATE TABLE `incident_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `incident_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `incident_approvals`
--

INSERT INTO `incident_approvals` (`id`, `incident_id`, `user_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, '2019-03-18 01:57:20', '2019-03-18 01:57:20'),
(2, 1, 3, 2, '2019-03-18 02:10:54', '2019-03-18 02:10:54'),
(3, 1, 3, 1, '2019-03-18 02:14:06', '2019-03-18 02:14:06'),
(4, 1, 3, 2, '2019-03-18 02:14:24', '2019-03-18 02:14:24');

-- --------------------------------------------------------

--
-- Table structure for table `incident_attachments`
--

CREATE TABLE `incident_attachments` (
  `id` int(11) NOT NULL,
  `incident_id` int(10) UNSIGNED NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incident_attachments`
--

INSERT INTO `incident_attachments` (`id`, `incident_id`, `attachment`, `name`, `alias`, `created_at`, `updated_at`) VALUES
(1, 1, 'attachments/180320190857306833bunga.jpeg', '180320190857306833bunga.jpeg', 'bunga.jpeg', '2019-03-18 01:57:30', '2019-03-18 01:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2014_10_12_100000_create_password_resets_table', 1),
(14, '2019_01_18_075248_create_layanan_tables', 1),
(15, '2019_01_21_090953_create_permission_tables', 1),
(16, '2019_01_22_075346_create_notifications_table', 1),
(17, '2019_02_14_015210_create_actions_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 4),
(1, 'App\\User', 5),
(1, 'App\\User', 6),
(1, 'App\\User', 6833),
(1, 'App\\User', 10112),
(1, 'App\\User', 10113),
(1, 'App\\User', 10114),
(1, 'App\\User', 10115),
(1, 'App\\User', 10116),
(1, 'App\\User', 10117),
(1, 'App\\User', 10696),
(1, 'App\\User', 10709),
(1, 'App\\User', 11420),
(2, 'App\\User', 3),
(3, 'App\\User', 10112),
(3, 'App\\User', 10696),
(3, 'App\\User', 10709),
(3, 'App\\User', 11420),
(4, 'App\\User', 5),
(5, 'App\\User', 10115),
(6, 'App\\User', 4),
(10, 'App\\User', 6),
(17, 'App\\User', 10112),
(18, 'App\\User', 10113),
(19, 'App\\User', 10114),
(20, 'App\\User', 10116),
(21, 'App\\User', 10117);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('19519816-03a5-418e-a74f-307be2a917d7', 'App\\Notifications\\IncidentCreated', 'App\\User', 3, '{\"stage_id\":9,\"id\":1,\"description\":\"Tidak bisa login sso\",\"url\":\"http:\\/\\/127.0.0.1:200\\/incident\\/1\\/show\"}', NULL, '2019-03-18 02:14:25', '2019-03-18 02:14:25'),
('43a0a105-e648-40fb-ac1c-dd07d4d2dd1f', 'App\\Notifications\\IncidentCreated', 'App\\User', 6833, '{\"stage_id\":6,\"id\":1,\"description\":\"Tidak bisa login sso\",\"url\":\"http:\\/\\/127.0.0.1:200\\/incident\\/1\\/approve\\/show\"}', '2019-03-18 02:14:25', '2019-03-18 02:14:06', '2019-03-18 02:14:25'),
('7ab532a4-4467-4ecc-8cd1-9a220d50a6bf', 'App\\Notifications\\IncidentCreated', 'App\\User', 6833, '{\"stage_id\":4,\"id\":1,\"description\":\"Tidak bisa login sso\",\"url\":\"http:\\/\\/127.0.0.1:200\\/incident\\/1\\/show\"}', '2019-03-18 02:11:21', '2019-03-18 02:10:54', '2019-03-18 02:11:21'),
('de473d3d-611a-44eb-8093-4ca868639990', 'App\\Notifications\\IncidentCreated', 'App\\User', 3, '{\"stage_id\":3,\"id\":1,\"description\":\"Tidak bisa login sso\",\"url\":\"http:\\/\\/127.0.0.1:200\\/incident\\/1\\/ticket\\/show\"}', '2019-03-18 02:10:55', '2019-03-18 01:57:23', '2019-03-18 02:10:55');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'browse request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(2, 'create request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(3, 'edit request', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(4, 'delete request', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(5, 'browse incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(6, 'create incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(7, 'edit incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(8, 'delete incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(9, 'employee incident', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(10, 'crud stages', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(11, 'crud statuses', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(12, 'crud services', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(13, 'boss approval', 'web', '2019-01-31 20:10:01', '2019-01-31 20:10:01'),
(14, 'operation approval', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02'),
(15, 'servicedesk update', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02'),
(16, 'so approval', 'web', '2019-01-31 20:10:02', '2019-01-31 20:10:02');

-- --------------------------------------------------------

--
-- Table structure for table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Urgent', '2019-03-17 17:00:00', '2019-03-17 17:00:00'),
(2, 'Medium', '2019-03-17 17:00:00', '2019-03-17 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_need` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_benefit` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `ticket` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  `reason` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `nda` int(1) DEFAULT NULL,
  `nda_date` timestamp NULL DEFAULT NULL,
  `so` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recomendation_status` int(1) DEFAULT NULL,
  `crf_category` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_actions`
--

CREATE TABLE `request_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL,
  `action_type` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_action_notes`
--

CREATE TABLE `request_action_notes` (
  `id` int(11) NOT NULL,
  `request_action_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_approvals`
--

CREATE TABLE `request_approvals` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `stage_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `request_attachments`
--

CREATE TABLE `request_attachments` (
  `id` int(10) NOT NULL,
  `request_id` int(10) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_reasonfiles`
--

CREATE TABLE `request_reasonfiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `request_id` int(10) DEFAULT NULL,
  `attachment` text,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `request_sos`
--

CREATE TABLE `request_sos` (
  `id` int(10) NOT NULL,
  `request_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `request_so_bps`
--

CREATE TABLE `request_so_bps` (
  `id` int(11) NOT NULL,
  `request_sos_id` int(10) NOT NULL,
  `attachment` text,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'employee', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(2, 'service desk', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(3, 'boss', 'web', '2019-01-31 20:09:59', '2019-01-31 20:09:59'),
(4, 'operation sd', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(5, 'so mes flat', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(6, 'so web', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(7, 'so mes long', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(8, 'so sap hr', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(9, 'so sap pp', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(10, 'operation ict', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(11, 'so sap fi', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(12, 'so sap co', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(13, 'so sap sd', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(14, 'so mes im', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(15, 'so mes mm', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(16, 'so sap ps', 'web', '2019-01-31 20:10:00', '2019-01-31 20:10:00'),
(17, 'manager beict', 'web', '2019-02-01 03:46:56', '2019-02-01 03:47:02'),
(18, 'coordinator aplikasi', 'web', '2019-02-03 10:12:07', '2019-02-03 10:12:11'),
(19, 'coordinator infra', 'web', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(20, 'so perangkat komputer', 'web', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(21, 'chief', 'web', '2019-02-11 17:00:00', '2019-02-12 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 17),
(2, 1),
(2, 17),
(3, 1),
(3, 17),
(4, 1),
(4, 17),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 2),
(11, 2),
(12, 2),
(13, 3),
(14, 4),
(14, 10),
(15, 2),
(16, 5),
(16, 6),
(16, 7),
(16, 8),
(16, 9),
(16, 11),
(16, 12),
(16, 13),
(16, 14),
(16, 16);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `groupserv` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `groupserv`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 'Layanan aplikasi sap MM', 'sap', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'Layanan aplikasi mes long', 'mes', 7, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'Layanan aplikasi web', '', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(4, 'Layanan aplikasi Office', '', 6, '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(5, 'Layanan messaging', '', 6, '2019-02-01 06:31:48', '2019-02-01 06:31:54'),
(6, 'Layanan perangkat komputer', '', 20, '2019-02-01 06:32:29', '2019-02-01 06:32:33'),
(7, 'Layanan printer', '', 6, '2019-02-01 06:32:58', '2019-02-01 06:33:03'),
(8, 'Layanan Jaringan', '', 6, '2019-02-01 06:33:26', '2019-02-01 06:33:30'),
(9, 'Layanan video conference', '', 6, '2019-02-01 06:34:15', '2019-02-01 06:34:22'),
(10, 'Layanan aplikasi mes flat', 'mes', 5, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(11, 'Layanan aplikasi sap PPQM', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(12, 'Layanan aplikasi sap FICO', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(13, 'Layanan aplikasi sap SD', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00'),
(14, 'Layanan aplikasi sap HR', 'sap', 6, '2019-02-14 17:00:00', '2019-02-14 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `service_coordinators`
--

CREATE TABLE `service_coordinators` (
  `id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  `service_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_coordinators`
--

INSERT INTO `service_coordinators` (`id`, `role_id`, `service_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 18, 1, 'Aplikasi', '2019-02-12 08:13:32', '2019-02-11 17:00:00'),
(2, 18, 2, 'Aplikasi', '2019-02-12 08:16:26', '2019-02-11 17:00:00'),
(3, 18, 3, 'Aplikasi', '2019-02-12 08:16:26', '2019-02-11 17:00:00'),
(4, 18, 10, 'aplikasi', '2019-02-16 14:05:25', '0000-00-00 00:00:00'),
(5, 19, 6, 'infra', '2019-02-17 10:22:42', '2019-02-16 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `severities`
--

CREATE TABLE `severities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `severities`
--

INSERT INTO `severities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'one user', '2019-03-17 17:00:00', '2019-03-17 17:00:00'),
(2, 'multiple user', '2019-03-17 17:00:00', '2019-03-17 17:00:00'),
(3, 'whole company', '2019-03-17 17:00:00', '2019-03-17 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `stages`
--

CREATE TABLE `stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stages`
--

INSERT INTO `stages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'waiting for Mgr/GM approval', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'waiting for operation desk', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'waiting for service desk', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(4, 'ticket created', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(5, 'resolved', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(6, 'waiting user confirmation', '2019-01-29 07:27:28', '2019-01-29 07:27:33'),
(7, 'waiting for operation ict', '2019-01-30 10:05:49', '2019-01-30 10:05:54'),
(8, 'request denied', '2019-01-30 10:35:37', '2019-01-30 10:35:43'),
(9, 'closed', '2019-01-30 10:49:03', '2019-01-30 10:49:12'),
(10, 'waiting for SO approval', '2019-01-23 13:57:15', '2019-01-30 13:57:20'),
(11, 'request rejected', '2019-02-01 04:13:35', '2019-02-08 04:13:40'),
(12, 'waiting for Manager BEICT', '2019-02-10 17:00:00', '2019-02-10 17:00:00'),
(13, 'waiting for coordinator so approval', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(14, 'waiting for bps from other service owners\r\n', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(15, 'waiting for so release', '2019-02-11 17:00:00', '2019-02-11 17:00:00'),
(16, 'waiting for boss approval for transport', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(17, 'Transport success', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(18, 'waiting for so transport', '2019-02-12 17:00:00', '2019-02-12 17:00:00'),
(19, 'waiting for chief approval', '2019-02-16 17:00:00', '2019-02-16 17:00:00'),
(20, 'waiting for detail information', '2019-02-17 17:00:00', '2019-02-17 17:00:00'),
(21, 'Approved', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(22, 'Change Failed', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(23, 'Change Successful', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(24, 'Denied', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(25, 'Implemented', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(26, 'Preparation', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(27, 'Recorded', '2019-02-27 17:00:00', '2019-02-27 17:00:00'),
(28, 'Waiting Recommendation Document', '2019-03-08 17:00:00', '2019-03-08 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'waiting for approval', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(2, 'approved', '2019-01-23 00:33:31', '2019-01-23 00:33:31'),
(3, 'rejected', '2019-01-23 00:33:31', '2019-01-23 00:33:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'ITA NURJANAH', 'sd@fmail.com', '$2y$10$8NeU0ilffIdzD0UsYGuPzeKoCefm9gUXxpwF/uq0x3w352TnyMkO2', 'DJ83IdkwdppE2Klz3iut8siuzIILEuwmkqTUBssNJPQmbKRwWDqaamnIyaPk', '2019-01-23 00:33:32', '2019-01-23 00:33:32'),
(4, 'RAHMADHANY TRIASTANTO', 'so@fmail.com', '$2y$10$1ulV41HzatLGU8uZ7t.7bOfBC4fGyVccUpTSeyV7aMYYnBLdqeBnO', '51W3a1dTzk9KP1zRquWYx8pxCmcIsAxMUW0Sv9zQh90Pyd4gPzYH4kEkPK7V', '2019-01-23 00:33:32', '2019-01-23 00:33:32'),
(5, 'ANDI SOFIA KARINA', 'spsd@fmail.com', '$2y$10$xvhQgX5JDIBA5t3SSpmWbOodbYz6xliIlHMFkgBVuAMb6/bXCfYP.', 'unZ4LOCYvJLa8Wco2aMaquEyZBYHKNRlaDH8rQWuuiw7mratvW0tuNODQ8SG', '2019-01-23 00:33:33', '2019-01-23 00:33:33'),
(6, 'OTTE DA', 'spict@fmail.com', '$2y$10$ecD/iKvERilg1v.edwBKJO9HQLHzu2EcJLb79AWtMMUrHGcZuFH0O', 'EcRlh00WoAvHwBsOOmKMSDnEqzu8iMf90DqXlCsMYe2iTKnDoN2oKJvi6Xie', '2019-01-23 00:33:33', '2019-01-23 00:33:33'),
(6833, 'UJANG FURKON', 'user@test.com', '$2y$10$W0lJHrCa4U45Gf0Mnnww9OnrBYZFTQyBwTisx..VMeIb24e1txyRK', 'iOQQVvKHsIoNkWlU6O2l3wE7WXYooymdEwYmNIuiEyCKZnSMLa6xZMBSEclT', '2019-01-23 00:33:32', '2019-01-25 00:29:32'),
(10112, 'MUHAMMAD HELMI NUR WIDIYARTO', 'el@fmail.com', '$2y$10$siWBJZrI4NfMPVnMq1ZaEOmbChtgrem25ecoK8MnWyAXrw6hdhHsq', 'SrdqglOvKjiozBtHu5eSNIg7XFVQgeG7pk0thYgznahH9l7MVd9MQmGAdQxe', '2019-01-23 00:33:32', '2019-01-23 19:02:56'),
(10113, 'coorditator apilkasi', 'coaplikasi@fmail.com', '$2y$10$1LZGE8Tw5s4QIbfYJWMbRedHL6ad/YgijGMLAfu3FSpQUDvQUPEPW', 'i4gnalN0O4Mtd87DugtU0QQXBes3HMLuSgpjXSuQdTW8KMQf2wSiV6KbFTpO', '2019-02-11 21:53:10', '2019-02-11 21:53:10'),
(10114, 'coinfra', 'coinfra@fmail.com', '$2y$10$PldJEdJLq5poNzun7sLFk.iWrn80AHY4ic0bcCwT6yV.ATmg0NfEC', 'UzXDu7huLSxwMRhOGDNPdqBiMWkP5VaV6Mc0evc7PUSjt3UriZ68ZyZYcKvv', '2019-02-11 21:54:26', '2019-02-11 21:54:26'),
(10115, 'Husni Mubarok', 'somesflat@fmail.com', '$2y$10$lkwssFhREaDojw8Vm84IMu3QUTcodV0E3agYVDV5TZaKy4YVT1ZnC', 'vBFIDdtLEOx2qykLNmTrIrfx3QENnhirX1eWME7km16S0fpnYj1owAXlvuh5', '2019-02-13 15:24:00', '2019-02-16 05:15:46'),
(10116, 'so perangkat komputer', 'sopk@fmail.com', '$2y$10$Vdd5zOll2ZAUiVGoUC3zYeRJ3Rgr2nAU2mJ8LQClSU1JZy21r8J6a', 'S4KHZpkxwhL5tt0kKjlH9I5OxwbmoKsUUAphf15o46qqTunlP4l6o7hrW4Bn', '2019-02-17 03:39:56', '2019-02-17 03:39:56'),
(10117, 'chief', 'chief@fmail.com', '$2y$10$kx3lEmOVh1vt5qHrdyjjHOn1MbtQ3MNWyRGIpw.D/GIdy/ZTY.o3m', 'IHXvueHdfXsqtKtTQdPSFpE55v8ZxKuYRGMiWP8F3i6vvHrfLkVnjCzxSzxe', '2019-02-17 08:31:44', '2019-02-17 08:31:44'),
(10696, 'WENI PURWANINGRUM', '', '$2y$10$bx9Yl1hIBg0He7BaP./V/ORiW0eYOEVPOqJ6mvxuItzp4MrH2NIQq', NULL, '2019-02-21 00:52:21', '2019-02-21 00:52:21'),
(10709, 'DICKY MARDIANA', 'dicky@fmail.com', '$2y$10$iPyL0Hhd2M2yJpkhr3N8R.gK9cyIpXBjTx0NR2v/2BWj/AnGB0ieq', NULL, '2019-02-21 01:15:54', '2019-02-21 01:15:54'),
(11420, 'JOKO RUSANDI AZHARI', 'joko@fmail.com', '$2y$10$SapA4crNWwG9t2rd2exWIutXCLlI.GPyAOxmx8.NI6qBbd0/q3Iqy', 'W4IFMWAGrknenoArqyDo2mOMZuOUJGycS9EcEV9RYSfvbZYwmHKFUVXudqYT', '2019-02-21 01:00:37', '2019-02-21 01:00:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incidents`
--
ALTER TABLE `incidents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_actions`
--
ALTER TABLE `incident_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_approvals`
--
ALTER TABLE `incident_approvals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incident_attachments`
--
ALTER TABLE `incident_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_actions`
--
ALTER TABLE `request_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_action_notes`
--
ALTER TABLE `request_action_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_approvals`
--
ALTER TABLE `request_approvals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_attachments`
--
ALTER TABLE `request_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_reasonfiles`
--
ALTER TABLE `request_reasonfiles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_sos`
--
ALTER TABLE `request_sos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_so_bps`
--
ALTER TABLE `request_so_bps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`,`role_id`);

--
-- Indexes for table `service_coordinators`
--
ALTER TABLE `service_coordinators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `severities`
--
ALTER TABLE `severities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stages`
--
ALTER TABLE `stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `incidents`
--
ALTER TABLE `incidents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `incident_actions`
--
ALTER TABLE `incident_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `incident_approvals`
--
ALTER TABLE `incident_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `incident_attachments`
--
ALTER TABLE `incident_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_actions`
--
ALTER TABLE `request_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_action_notes`
--
ALTER TABLE `request_action_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_approvals`
--
ALTER TABLE `request_approvals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_attachments`
--
ALTER TABLE `request_attachments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_reasonfiles`
--
ALTER TABLE `request_reasonfiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_sos`
--
ALTER TABLE `request_sos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `request_so_bps`
--
ALTER TABLE `request_so_bps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `service_coordinators`
--
ALTER TABLE `service_coordinators`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `severities`
--
ALTER TABLE `severities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stages`
--
ALTER TABLE `stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11421;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
